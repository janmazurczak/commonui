//
//  AdaptiveVerticalCollectionViewController.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 12/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class AdaptiveVerticalFlowLayoutCollectionViewController: UICollectionViewController {
    
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        setupLayoutAsAdaptiveVerticalIfChanged(traitCollection: traitCollection, size: view.bounds.size)
    }
    
    
    open override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        targetTraitCollectionInCurrentTransition = newCollection
    }
    
    
    var targetTraitCollectionInCurrentTransition: UITraitCollection?
    
    
    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        targetTraitCollectionInCurrentTransition = nil
        setupLayoutAsAdaptiveVerticalIfChanged(traitCollection: traitCollection, size: view.bounds.size)
    }
    
    
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        setupLayoutAsAdaptiveVerticalIfChanged(traitCollection: targetTraitCollectionInCurrentTransition ?? traitCollection, size: size)
        collectionView?.invalidateLayoutAlongsideTransition(transitionCoordinator: coordinator)
    }
    
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupLayoutAsAdaptiveVerticalIfChanged(traitCollection: traitCollection, size: view.bounds.size)
    }
    
    
    private var adaptiveLayoutTraitCollection: UITraitCollection?
    private var adaptiveLayoutSize: CGSize = .zero
    open func setupLayoutAsAdaptiveVerticalIfChanged(traitCollection: UITraitCollection, size: CGSize) {
        if adaptiveLayoutTraitCollection?.horizontalSizeClass == traitCollection.horizontalSizeClass && adaptiveLayoutSize.width == size.width && adaptiveLayoutSize.height == size.height {
            return
        }
        flowLayout?.setupAsAdaptiveVertical(traitCollection: traitCollection, size: size, layoutConfiguration: layoutConfiguration())
        adaptiveLayoutTraitCollection = traitCollection
        adaptiveLayoutSize = size
    }
    
    
    open func layoutConfiguration() -> LayoutConfiguration {
        return LayoutConfiguration(
            spacing: 4,
            compactCellAspectRatio: 0.25,
            regularCellEstimatedWidth: 180,
            regularCellAspectRatio: 1.0)
    }
    
    
    public struct LayoutConfiguration {
        public var spacing: CGFloat
        public var compactCellHeight: CGFloat?
        public var compactCellAspectRatio: CGFloat?
        public var regularCellEstimatedWidth: CGFloat
        public var regularCellAspectRatio: CGFloat
        
        
        public init(spacing: CGFloat, compactCellHeight: CGFloat? = nil, compactCellAspectRatio: CGFloat? = nil, regularCellEstimatedWidth: CGFloat, regularCellAspectRatio: CGFloat) {
            self.spacing = spacing
            self.compactCellHeight = compactCellHeight
            self.compactCellAspectRatio = compactCellAspectRatio
            self.regularCellEstimatedWidth = regularCellEstimatedWidth
            self.regularCellAspectRatio = regularCellAspectRatio
        }
        
        
        public func compactCellSize(forContainerSize size: CGSize) -> CGSize? {
            if let compactCellHeight = compactCellHeight {
                return CGSize(
                    width: size.width,
                    height: compactCellHeight)
            } else if let compactCellAspectRatio = compactCellAspectRatio {
                return CGSize(
                    width: size.width,
                    height: min(size.width * compactCellAspectRatio, size.height))
            } else {
                return nil
            }
        }
        
        
        public func regularCellSize(forContainerSize size: CGSize) -> CGSize {
            let numberOfItemsInRow = floor(size.width / regularCellEstimatedWidth)
            let itemWidth = floor((size.width - spacing) / numberOfItemsInRow) - spacing
            return CGSize(
                width: itemWidth,
                height: min(itemWidth * regularCellAspectRatio, size.height))
        }
    }
    
}


extension UICollectionViewFlowLayout {
    
    public func setupAsAdaptiveVertical(traitCollection: UITraitCollection, size: CGSize, layoutConfiguration: AdaptiveVerticalFlowLayoutCollectionViewController.LayoutConfiguration) {
        
        if
            traitCollection.horizontalSizeClass == .compact,
            let compactCellSize = layoutConfiguration.compactCellSize(forContainerSize: size)
        {
            setupAsStandardVerticalLayout(spacing: layoutConfiguration.spacing)
            itemSize = compactCellSize
        } else {
            setupAsStandardVerticalLayout(
                spacing: layoutConfiguration.spacing,
                sideMargin: layoutConfiguration.spacing)
            itemSize = layoutConfiguration.regularCellSize(forContainerSize: size)
        }
        
    }
    
}


extension UICollectionView {
    
    
    public func invalidateLayoutAlongsideTransition(transitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        collectionViewLayout.invalidateLayout()
        coordinator.animate(alongsideTransition: { _ in
            self.layoutIfNeeded()
            }, completion: nil)
    }
    
    
}
