//
//  SceneUserInterface.swift
//  Yoox
//
//  Created by Jan Mazurczak on 10/12/15.
//  Copyright © 2015 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class UserInterface: NSObject, FadeInAndOutController, UITraitEnvironment {
    
    
    fileprivate weak var presentationView: UIView?
    fileprivate var keyedSubviews = [String : UIView]()
    fileprivate var constraints = [NSLayoutConstraint]() {
        willSet { NSLayoutConstraint.deactivate(constraints) }
        didSet { NSLayoutConstraint.activate(constraints) }
    }
    
    
    public init(traitCollection: UITraitCollection) {
        self.traitCollection = traitCollection
        super.init()
    }
    
    
    open var isPresented: Bool { return presentationView != nil }
    
    
    open func fadeInAndOutChildControllers() -> [FadeInAndOutController] {
        return []
    }
    
    
    open func fadeInAndOutAwareItems() -> [FadeInAndOut] {
        return keyedSubviews.compactMap { $0.value as? FadeInAndOut }
    }
    
    
    /// You should override this function
    open func createSubviews() -> [String:UIView] {
        return [:]
    }
    
    
    /// You should override this function
    open func createConstraints(keyedSubviews: [String: UIView], superview: UIView) -> [NSLayoutConstraint] {
        return []
    }
    
    
    open func present(in view: UIView, initialDelay: TimeInterval = 0, duration: TimeInterval = 0.5) {
        if presentationView == view { return }
        if isPresented { dismiss() }
        presentationView = view
        
        let keyedSubviews = createSubviews()
        self.keyedSubviews = keyedSubviews
        
        keyedSubviews.forEach { view.addSubview($0.value) }
        
        constraints = createConstraints(keyedSubviews: keyedSubviews, superview: view)
        
        prepareToFadeIn()
        fadeIn(initialDelay: initialDelay, duration: duration, completion: nil)
    }
    
    
    open func dismiss(initialDelay: TimeInterval = 0, duration: TimeInterval = 0.5) {
        presentationView = nil
        
        let subviews = self.keyedSubviews
        guard subviews.count > 0 else { return }
        
        subviews.forEach { $0.value.isUserInteractionEnabled = false }
        
        fadeOut(initialDelay: initialDelay, duration: duration) { finished in
            subviews.forEach { $0.value.removeFromSuperview() }
            guard self.isPresented == false else { return }
            self.constraints = []
            self.keyedSubviews = [:]
        }
    }
    
    
    public var traitCollection: UITraitCollection { didSet { traitCollectionDidChange(oldValue) } }
    
    
    open func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        refreshConstraints()
    }
    
    
    public func refreshConstraints() {
        guard let presentationView = presentationView else { return }
        constraints = createConstraints(keyedSubviews: keyedSubviews, superview: presentationView)
    }
    
    
}


extension UIViewController {
    
    
    open var userInterfaceRefreshingDuration: TimeInterval { return 0.5 }
    
    
    public func refreshUserInterface(_ userInterface: UserInterface, visible: Bool) {
        if visible {
            userInterface.present(in: view, duration: userInterfaceRefreshingDuration)
        } else {
            userInterface.dismiss(duration: userInterfaceRefreshingDuration)
        }
    }
    
}

