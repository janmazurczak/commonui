//
//  Created in Playgrounds Squire
//  https://itunes.apple.com/pl/app/playgrounds-squire/id1341463239?mt=8
//  Copyright © 2018 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import UIKit


open class PopContainerViewController<ContentVC>: UIViewController, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning where ContentVC: UIViewController {
    
    
    public let content: ContentVC
    public let backgroundStyle: UIBlurEffect.Style?
    public let backgroundTint: UIColor
    
    
    public init(content: ContentVC, backgroundStyle: UIBlurEffect.Style? = .dark, backgroundTint: UIColor = .clear) {
        self.content = content
        self.backgroundStyle = backgroundStyle
        self.backgroundTint = backgroundTint
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
        
        addChild(content)
        content.didMove(toParent: self)
    }
    
    
    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    open override func loadView() {
        let view = UIVisualEffectView(effect: backgroundStyle == nil ? nil : UIBlurEffect(style: backgroundStyle!))
        view.contentView.backgroundColor = backgroundTint
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.contentView.addSubview(content.view)
        content.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            content.view.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            content.view.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
        
        self.view = view
    }
    
    
    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if isViewLoaded && view.window == nil {
            unloadView()
        }
    }
    
    
    open func unloadView() {
        view = nil
    }
    
    
    open var isBackgroundPresented: Bool = false {
        didSet {
            (view as! UIVisualEffectView).effect = (isBackgroundPresented && backgroundStyle != nil) ? UIBlurEffect(style: backgroundStyle!) : nil
            backgroundContentView.backgroundColor = isBackgroundPresented ? backgroundTint : .clear
        }
    }
    
    
    public var backgroundContentView: UIView {
        return (view as! UIVisualEffectView).contentView
    }
    
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }
    
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let dismissing = transitionContext.viewController(forKey: .from) == self
        
        animatedTransitionPrepare(containerView: transitionContext.containerView, dismissing: dismissing)
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animate(
            withDuration: duration,
            delay: 0,
            usingSpringWithDamping: 0.6,
            initialSpringVelocity: 0,
            options: .allowUserInteraction,
            animations: { self.animatedTransition(dismissing: dismissing) },
            completion: { finish in
                self.animatedTransitionEnd(dismissing: dismissing)
                transitionContext.completeTransition(finish)
        })
    }
    
    
    func animatedTransitionPrepare(containerView: UIView, dismissing: Bool) {
        if !dismissing {
            (view as? UIVisualEffectView)?.effect = nil
            backgroundContentView.backgroundColor = .clear
            containerView.addSubview(view)
            NSLayoutConstraint.activate([
                view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
                view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
                view.topAnchor.constraint(equalTo: containerView.topAnchor),
                view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
                ])
            content.view.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        }
    }
    
    
    func animatedTransition(dismissing: Bool) {
        if dismissing {
            isBackgroundPresented = false
            content.view.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        } else {
            isBackgroundPresented = true
            content.view.transform = CGAffineTransform.identity
        }
    }
    
    
    func animatedTransitionEnd(dismissing: Bool) {
        if dismissing {
            view.removeFromSuperview()
        }
    }
    
    
}

