//
//  Created in Playgrounds Squire
//  https://itunes.apple.com/pl/app/playgrounds-squire/id1341463239?mt=8
//  Copyright © 2018 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import UIKit

public protocol UnderlinedSectionLabelDelegate: class {
    func didSelect(underlinedSection: UnderlinedHorizontalSectionLabel, index: Int)
}

public class UnderlinedHorizontalSectionLabel: UIVisualEffectView {
    
    public var titles = [String]() {
        didSet {
            setupTitleLabels()
        }
    }
    
    private let scrollView = UIScrollView(frame: .zero)
    private let stackView = UIStackView(arrangedSubviews: [])
    public let underline = UIView(frame: .zero)
    
    public var margin: CGFloat = 20 {
        didSet { setNeedsLayout() }
    }
    
    public weak var delegate: UnderlinedSectionLabelDelegate?
    
    public init(style: UIBlurEffect.Style? = .light) {
        super.init(effect: style == nil ? nil : UIBlurEffect(style: style!))
        setupOnce()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupOnce()
    }
    
    private func setupOnce() {
        translatesAutoresizingMaskIntoConstraints = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        underline.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.isBaselineRelativeArrangement = true
        stackView.spacing = 20
        scrollView.addSubview(stackView)
        scrollView.addSubview(underline)
        contentView.addSubview(scrollView)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.alwaysBounceHorizontal = true
        underline.backgroundColor = .black
        underline.layer.zPosition = 100
        NSLayoutConstraint.activate([
            underline.heightAnchor.constraint(equalToConstant: 1.5),
            
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            
            stackView.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
            
            scrollView.topAnchor.constraint(equalTo: contentView.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
            ])
        
    }
    
    private var underlineConstraints = [NSLayoutConstraint]() {
        willSet { underlineConstraints.forEach { $0.isActive = false } }
        didSet { underlineConstraints.forEach { $0.isActive = true } }
    }
    
    private var labels = [UIButton]() {
        willSet { labels.forEach { stackView.removeArrangedSubview($0) } }
        didSet {
            labels.forEach {
                stackView.addArrangedSubview($0)
                $0.addTarget(self, action: #selector(self.select(button:)), for: .touchUpInside)
            }
        }
    }
    
    @objc func select(button: UIButton) {
        guard let index = labels.firstIndex(of: button) else { return }
        select(index: index, animated: true)
        delegate?.didSelect(underlinedSection: self, index: index)
    }
    
    private func setupTitleLabels() {
        labels = titles.map {
            let button = UIButton(frame: .zero)
            button.setTitle($0, for: .normal)
            button.setTitleColor(.black, for: .normal)
            button.setTitleColor(#colorLiteral(red: 0.600000023841858, green: 0.600000023841858, blue: 0.600000023841858, alpha: 1.0), for: .highlighted)
            return button
        }
    }
    
    public var relativeOffset: CGFloat {
        set {
            var x: CGFloat = 0
            if let selected = selectedIndex, selected < labels.count && selected >= 0 {
                let nextIndex = max(0, min(newValue > 0 ? selected + 1 : selected - 1, labels.count - 1))
                if selected != nextIndex {
                    let label = labels[selected]
                    let nextLabel = labels[nextIndex]
                    x = abs(newValue) * (nextLabel.center.x - label.center.x)
                }
            }
            underline.transform = CGAffineTransform(translationX: x, y: 0)
        }
        get {
            let underlineX = (underline.layer.presentation() ?? underline.layer).transform.m41
            if let selected = selectedIndex, selected < labels.count && selected >= 0 {
                let nextIndex = max(0, min(underlineX > 0 ? selected + 1 : selected - 1, labels.count - 1))
                if selected != nextIndex {
                    let label = labels[selected]
                    let nextLabel = labels[nextIndex]
                    return underlineX / abs(nextLabel.center.x - label.center.x)
                }
            }
            return 0
        }
    }
    
    public var selectedIndex: Int? {
        didSet {
            setupSelectedIndex()
        }
    }
    
    public func select(index: Int?, animated: Bool) {
        selectedIndex = index
        UIView.animate(withDuration: 0.3) {
            self.relativeOffset = 0
            self.layoutIfNeeded()
        }
        guard let selectedButton = selectedButton else { return }
        scrollView.scrollRectToVisible(scrollView.convert(selectedButton.frame, from: selectedButton.superview), animated: animated)
    }
    
    private var selectedButton: UIButton? {
        guard let selectedIndex = selectedIndex else { return nil }
        guard selectedIndex < labels.count && selectedIndex >= 0 else { return nil }
        return labels[selectedIndex]
    }
    
    private func setupSelectedIndex() {
        var constraints: [NSLayoutConstraint] = [
            underline.widthAnchor.constraint(equalToConstant: 0)
        ]
        defer { underlineConstraints = constraints }
        guard let label = selectedButton else { return }
        constraints = [
            underline.leadingAnchor.constraint(equalTo: label.leadingAnchor),
            underline.trailingAnchor.constraint(equalTo: label.trailingAnchor),
            underline.topAnchor.constraint(equalTo: label.lastBaselineAnchor, constant: 4)
        ]
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        setupContentInset()
    }
    
    private func setupContentInset() {
        let inset = max(margin, 0.5 * (scrollView.bounds.size.width - scrollView.contentSize.width))
        scrollView.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    
}

