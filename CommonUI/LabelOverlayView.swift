//
//  LabelOverlayView.swift
//  Amama
//
//  Created by Jan Mazurczak on 09/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class LabelOverlayView: UIVisualEffectView {
    
    
    public let label = UILabel()
    
    
    public init(message: String) {
        super.init(effect: UIBlurEffect(style: .light))
        
        setupOnce()
        label.text = message
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupOnce() {
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .darkGray
        label.textAlignment = .center
        label.numberOfLines = 0
        
        label.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(label)
        label.addConstraintsToFitInSuperview(with: 3)
        _ = label.addConstraintsToCenterInSuperview()
    }
    
    
    open func fadeIn(_ container: UIView) {
        
        label.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        effect = nil
        translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(self)
        addConstraintsToFillSuperview()
        
        UIView.animate(
            withDuration: 0.8,
            delay: 0,
            usingSpringWithDamping: 0.6,
            initialSpringVelocity: 0,
            options: [.allowUserInteraction, .beginFromCurrentState],
            animations: {
                self.effect = UIBlurEffect(style: .light)
                self.label.transform = CGAffineTransform.identity
            },
            completion: nil)
    }
    
    
    open func fadeOut() {
        UIView.animate(
            withDuration: 0.4,
            delay: 0,
            usingSpringWithDamping: 1.0,
            initialSpringVelocity: -10,
            options: [.allowUserInteraction, .beginFromCurrentState],
            animations: {
                self.effect = nil
                self.label.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
            },
            completion: { _ in self.removeFromSuperview() })
    }
    
    
    
}
