//
//  IconButton.swift
//
//  Created by Jan Mazurczak on 22/08/15.
//  Copyright © 2015 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class IconButton: UIButton {
    
    public convenience init(iconShape: UIBezierPath, tintColor: UIColor? = .clear) {
        self.init()
        
        shape = iconShape
        iconLayer.path = iconShape.cgPath
        layer.addSublayer(iconLayer)
        translatesAutoresizingMaskIntoConstraints = false
        self.tintColor = tintColor
        
        setupIconLayerAccordingToTintColor()
    }
    
    fileprivate(set) lazy var iconLayer = CAShapeLayer()
    
    fileprivate func setupIconLayerAccordingToTintColor() {
        if tintColor == UIColor.clear {
            iconLayer.fillColor = UIColor.clear.cgColor
            iconLayer.lineWidth = 1.0
            iconLayer.strokeColor = UIColor.black.cgColor
        } else {
            iconLayer.fillColor = tintColor.cgColor
            iconLayer.lineWidth = 0.0
            iconLayer.strokeColor = UIColor.clear.cgColor
        }
    }
    
    open var shape: UIBezierPath? {
        didSet {
            iconLayer.path = shape?.cgPath
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        let iconSize = shape?.bounds.size ?? CGSize.zero
        let size = max(max(iconSize.width, iconSize.height), 44.0)
        return CGSize(width: size, height: size)
    }
    
    open override func tintColorDidChange() {
        super.tintColorDidChange()
        setupIconLayerAccordingToTintColor()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        iconLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    open override var isHighlighted: Bool {
        didSet {
            iconLayer.transform = isHighlighted ? CATransform3DMakeScale(0.85, 0.85, 1.0) : CATransform3DIdentity
        }
    }
    
}

extension IconButton {
    
    public class func ActionKeyedButtons(_ keys: [String], target: AnyObject, tintColor: UIColor?, shapes: Shapes) -> [String: UIButton] {
        return ActionKeyedButtons(keys, target: target, creator: { IconButton(iconShape: shapes[$0]!, tintColor: tintColor) })
    }
    
}
