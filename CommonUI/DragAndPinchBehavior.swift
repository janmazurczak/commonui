//
//  Copyright (c) 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class DragAndPinchBehavior: UIDynamicBehavior {
    
    
    public let item: UIDynamicItemWithSettableBounds
    var attachments: [UIAttachmentBehavior] = []
    
    
    init(item inputItem: UIDynamicItemWithSettableBounds) {
        item = inputItem
        super.init()
    }
    
    
    func updateSize(gesture: DragAndPinchGesture) {
        if let scale = gesture.scale {
            item.bounds = CGRect(x: 0, y: 0, width: item.bounds.size.width * scale, height: item.bounds.size.height * scale)
            gesture.resetScale()
        }
    }
    
    
    func updateAnchorPoints(gesture: DragAndPinchGesture, referenceView: UIView) {
        if (gesture.state == .ended) || (gesture.state == .cancelled) {
            updateAnchorPoints([])
        } else {
            updateAnchorPoints(gesture.allTouchesPoints(referenceView))
        }
    }
    
    
    func updateAnchorPoints(_ points: [CGPoint]) {
        if points.count != attachments.count {
            let b = item.bounds
            let c = item.center
            while attachments.count > 0 {
                removeChildBehavior(attachments.last!)
                attachments.removeLast()
            }
            item.bounds = b
            item.center = c
        }
        
        var i = 0
        for point in points {
            if i < attachments.count {
                let attachment = attachments[i]
                attachment.anchorPoint = point;
            } else {
                let pointOffset = CGPoint(x: point.x-item.center.x, y: point.y-item.center.y)
                let pointTransformed = pointOffset.applying(item.transform.inverted())
                let offset = UIOffset(horizontal: pointTransformed.x, vertical: pointTransformed.y)
                let attachment = UIAttachmentBehavior(item: item, offsetFromCenter: offset, attachedToAnchor: point)
                attachments.append(attachment)
                addChildBehavior(attachment)
            }
            i += 1
        }
    }
    
    
}


public protocol UIDynamicItemWithSettableBounds: UIDynamicItem {
    var bounds: CGRect { get set }
}


extension UIView: UIDynamicItemWithSettableBounds {}

