//
//  Shapes.swift
//
//  Created by Jan Mazurczak on 18/07/14.
//  Copyright (c) 2014 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit

/*
Example format parsable by this class below.

Note that demanded shapes should be always grouped by <g id="shapeName"> where shapeName is key used to find one UIBezierPath in the collection of shapes.

Software which exports this format nicely is iDraw available in Mac App Store.


<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0" y="0" width="640" height="480" viewBox="0, 0, 640, 480">
<g id="buttons">
<g id="add">
<path d="M139.536,54.074 L163.175,54.074" fill-opacity="0" stroke="#000000" stroke-width="1"/>
<path d="M151.355,65.894 L151.355,42.255" fill-opacity="0" stroke="#000000" stroke-width="1"/>
</g>
<g id="trash">
<path d="M187.052,48.573 L187.052,63.409 z" fill-opacity="0" stroke="#000000" stroke-width="1"/>
<path d="M190.785,48.573 L190.007,63.409 z" fill-opacity="0" stroke="#000000" stroke-width="1"/>
<path d="M183.287,48.573 L184.07,63.409 z" fill-opacity="0" stroke="#000000" stroke-width="1"/>
<path d="M195.903,46.185 L178.174,46.185 L178.174,46.195 L179.473,46.195 L180.926,64.09 C180.926,65.088 181.732,65.894 182.729,65.894 L191.41,65.894 C192.407,65.894 193.213,65.088 193.213,64.09 L194.648,46.195 L195.903,46.195 L195.903,46.185 z M183.998,44.058 C183.998,43.56 184.404,43.154 184.896,43.154 L189.175,43.154 C189.674,43.154 190.074,43.56 190.074,44.058 L190.074,46.185 L183.998,46.185 L183.998,44.058 z" fill-opacity="0" stroke="#000000" stroke-width="1"/>
</g>
<g id="share">
<path d="M220.001,57.331 L220.001,41.174" fill-opacity="0" stroke="#000000" stroke-width="1"/>
<path d="M216.002,44.862 L220.018,40.924 L223.815,44.799" fill-opacity="0" stroke="#000000" stroke-width="1"/>
<path d="M216.957,49.964 L211.997,49.964 L211.997,66 L228,66 L228,49.997 L223.04,49.964" fill-opacity="0" stroke="#000000" stroke-width="1"/>
</g>
<g id="dismiss">
<path d="M249.03,66 L268,47.03" fill-opacity="0" stroke="#000000" stroke-width="1"/>
<path d="M268,66 L249.03,47.03" fill-opacity="0" stroke="#000000" stroke-width="1"/>
</g>
</g>
</svg>


*/

/// Use this class to parse grid of icons delivered as SVG by your designer. You will get a collection of centered to 0,0 icons as UIBezierPath. DEPRECATED as Xcode is now supporting PDF assets.
open class Shapes: NSObject, XMLParserDelegate {
    
    public static let InterfaceIcons = Shapes(svgFilename: "InterfaceIcons")
    
    fileprivate var shapes: [String : UIBezierPath]
    
    public init(svgFilename: String) {
        let sourceURL = Bundle.main.url(forResource: svgFilename, withExtension: "svg")!
        shapes = [:]
        super.init()
        
        let parser = XMLParser(contentsOf: sourceURL)
        parser!.delegate = self
        parser!.parse()
        normaliseShapes()
    }
    
    open subscript (key: String) -> UIBezierPath? {
        get {
            return shapes[key] ?? shapes["unknown"]
        }
        set(value) {
            shapes[key] = value
        }
    }
    
    open var allShapes: [(String, UIBezierPath)] {
        var results: [(String, UIBezierPath)] = []
        for (key, shape) in shapes {
            results.append((key, shape))
        }
        return results
    }
    
    func normaliseShapes() {
        for (_, bezierPath) in shapes {
            bezierPath.normalisePath()
        }
    }
    
    var currenltyParsingShapeID: String?
    open func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        if elementName == "g" {
            currenltyParsingShapeID = attributeDict["id"]
        }
        if elementName == "path" {
            
            var bezierPath = shapes[currenltyParsingShapeID!]
            if bezierPath == nil {
                bezierPath = UIBezierPath()
            }
            if let svgCode = attributeDict["d"] {
                bezierPath!.addSVG(svgCode as NSString)
                shapes[currenltyParsingShapeID!] = bezierPath!
            }
        }
    }
    
}

private let svgCommandsSet = CharacterSet(charactersIn: "MmLlHhVvCcSsQqTtZz")
private let svgArgumentRegex = try! NSRegularExpression(pattern: "[-+]?[0-9]*\\.?[0-9]*", options: [])
private let svgNumberOfArgumentsForCommand: [String: Int] = [
    "m": 2, "l": 2, "h": 1, "v": 1, "c": 6, "s": 4, "q": 4, "t": 2, "z": 1]

extension UIBezierPath {
    
    public func normalisePath() {
        let b = bounds
        let transform = CGAffineTransform(
            translationX: -b.origin.x-0.5*b.size.width,
            y: -b.origin.y-0.5*b.size.height);
        apply(transform)
    }
    
    func addSVG(_ svgCode: NSString) {
        var lastControlPoint = CGPoint.zero
        var searchingRange = NSRange(location: 0, length: svgCode.length)
        
        while (searchingRange.length > 0) {
            let commandRange = svgCode.rangeOfCharacter(from: svgCommandsSet, options: [], range: searchingRange)
            if commandRange.location == NSNotFound {
                break
            }
            
            let command = svgCode.substring(with: commandRange)
            var nextCommandRange = svgCode.rangeOfCharacter(
                from: svgCommandsSet, options: [],
                range: NSRange(location: commandRange.location+1, length: svgCode.length-commandRange.location-1))
            if nextCommandRange.location == NSNotFound {
                nextCommandRange.location = svgCode.length
            }
            
            var checkingResults: [NSTextCheckingResult] = []
            if NSInteger(nextCommandRange.location) - NSInteger(commandRange.location) - 1 > 0 {
                checkingResults = svgArgumentRegex.matches(
                    in: svgCode as String, options: [],
                    range: NSRange(
                        location: commandRange.location+1,
                        length: nextCommandRange.location-commandRange.location-1))
            }
            
            var arguments: [String] = []
            for checkingResult in checkingResults {
                if checkingResult.range.length > 0 {
                    arguments.append(svgCode.substring(with: checkingResult.range))
                }
            }
            
            lastControlPoint = self.addSVGCommand(
                command,
                arguments: arguments,
                lastControlPoint: lastControlPoint)

            searchingRange = NSRange(
                location: nextCommandRange.location,
                length: svgCode.length-nextCommandRange.location)
        }
    }
    
    func addSVGCommand(_ command: String, arguments: [String], lastControlPoint: CGPoint) -> CGPoint {
        
        var lcPoint = lastControlPoint
        
        let c = command.lowercased()
        let isRelative = (command == c)
        func offsetPoint(_ point: CGPoint) -> CGPoint {
            if isRelative {
                let currentPoint = self.currentPoint
                return CGPoint(x: currentPoint.x + point.x, y: currentPoint.y + point.y)
            } else {
                return point
            }
        }
        
        let numberOfArguments = svgNumberOfArgumentsForCommand[c]!
        func loopedAction(_ action: (_ args:[String]) -> Void) -> Void {
            var argOffset = 0
            while argOffset < arguments.count {
                let argEndOffset = numberOfArguments + argOffset
                let subArguments = arguments[argOffset ..< argEndOffset]
                action(Array(subArguments))
                argOffset = argOffset + numberOfArguments
            }
        }
        
        switch c {
        case "m":
            loopedAction({ (args) -> Void in
                let point = CGPoint(
                    x: Double(args[0])!,
                    y: Double(args[1])!)
                lcPoint = offsetPoint(point)
                self.move(to: lcPoint)
            })
        case "l":
            loopedAction({ (args) -> Void in
                let point = CGPoint(
                    x: Double(args[0])!,
                    y: Double(args[1])!)
                lcPoint = offsetPoint(point)
                self.addLine(to: lcPoint)
            })
        case "h":
            loopedAction({ (args) -> Void in
                let point = CGPoint(
                    x: Double(args[0])!,
                    y: isRelative ? 0.0 : Double(self.currentPoint.y))
                lcPoint = offsetPoint(point)
                self.addLine(to: lcPoint)
            })
        case "v":
            loopedAction({ (args) -> Void in
                let point = CGPoint(
                    x: isRelative ? 0.0 : Double(self.currentPoint.x),
                    y: Double(args[0])!)
                lcPoint = offsetPoint(point)
                self.addLine(to: lcPoint)
            })
        case "c":
            loopedAction({ (args) -> Void in
                let cpoint1 = CGPoint(
                    x: Double(args[0])!,
                    y: Double(args[1])!)
                let cpoint2 = CGPoint(
                    x: Double(args[2])!,
                    y: Double(args[3])!)
                let point = CGPoint(
                    x: Double(args[4])!,
                    y: Double(args[5])!)
                lcPoint = offsetPoint(cpoint2)
                self.addCurve(
                    to: offsetPoint(point),
                    controlPoint1: offsetPoint(cpoint1),
                    controlPoint2: lcPoint)
            })
        case "s":
            loopedAction({ (args) -> Void in
                let cpoint1 = CGPoint(
                    x: 2.0*(self.currentPoint.x - lcPoint.x) + lcPoint.x,
                    y: 2.0*(self.currentPoint.y - lcPoint.y) + lcPoint.y)
                let cpoint2 = CGPoint(
                    x: Double(args[0])!,
                    y: Double(args[1])!)
                let point = CGPoint(
                    x: Double(args[2])!,
                    y: Double(args[3])!)
                lcPoint = offsetPoint(cpoint2)
                self.addCurve(
                    to: offsetPoint(point),
                    controlPoint1: cpoint1,
                    controlPoint2: lcPoint)
            })
        case "q":
            loopedAction({ (args) -> Void in
                let cpoint1 = CGPoint(
                    x: Double(args[0])!,
                    y: Double(args[1])!)
                let point = CGPoint(
                    x: Double(args[2])!,
                    y: Double(args[3])!)
                lcPoint = offsetPoint(cpoint1)
                self.addQuadCurve(
                    to: offsetPoint(point),
                    controlPoint: lcPoint)
            })
        case "t":
            loopedAction({ (args) -> Void in
                let point = CGPoint(
                    x: Double(args[0])!,
                    y: Double(args[1])!)
                lcPoint = CGPoint(
                    x: 2.0*(self.currentPoint.x - lcPoint.x) + lcPoint.x,
                    y: 2.0*(self.currentPoint.y - lcPoint.y) + lcPoint.y)
                self.addQuadCurve(
                    to: offsetPoint(point),
                    controlPoint: lcPoint)
            })
        case "z":
            self.close()
        default:
            print("unknown svg command: \(command)")
        }
        
        return lcPoint
    }
}

extension UIBezierPath {
    
    public func imageDrawingShapeCenteredWithSize(_ size: CGSize, fill: Bool) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        let c = UIGraphicsGetCurrentContext()
        c?.translateBy(x: size.width*0.5 - bounds.midX,
            y: size.height*0.5 - bounds.midY)
        if fill {
            self.fill()
        } else {
            self.stroke()
        }
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
}
