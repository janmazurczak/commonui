//
//  Created in Playgrounds Squire
//  https://itunes.apple.com/pl/app/playgrounds-squire/id1341463239?mt=8
//  Copyright © 2018 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import UIKit

public class HorizontalSectionsController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UnderlinedSectionLabelDelegate, UIGestureRecognizerDelegate {
    
    public var sections = [UIViewController]() {
        didSet {
            let index = currentSectionIndex() ?? 0
            setViewControllers([section(at: index)].compactMap{ $0 }, direction: .forward, animated: false, completion: nil)
            applyTitlesToLabels()
        }
    }
    
    public init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        dataSource = self
        delegate = self
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func currentSectionIndex() -> Int? {
        guard let section = viewControllers?.first else { return nil }
        return index(of: section)
    }
    
    func index(of viewController: UIViewController) -> Int? {
        return sections.firstIndex(where: { $0 === viewController })
    }
    
    func section(at index: Int) -> UIViewController? {
        guard (index >= 0) && (index < sections.count) else { return nil }
        return sections[index]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = index(of: viewController) {
            return section(at: index - 1)
        } else {
            return section(at: 0)
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = index(of: viewController) {
            return section(at: index + 1)
        } else {
            return section(at: 0)
        }
    }
    
    // underlined section labels
    
    private var isTrackingSwipe: Bool = false
    
    @objc private func swipeTracking(pan: UIPanGestureRecognizer) {
        if pan.state == .began || !isTrackingSwipe {
            pan.setTranslation(CGPoint(x: (labels?.relativeOffset ?? 0) * -view.bounds.size.width, y: 0), in: view)
        }
        if isTrackingSwipe {
            let x = pan.translation(in: view).x / view.bounds.size.width
            if pan.state == .ended || pan.state == .cancelled {
                let v = max(-1, min(2 * pan.velocity(in: view).x / view.bounds.size.width, 1))
                UIView.animate(withDuration: 0.3) {
                    self.labels?.relativeOffset = -(x + v)
                }
            } else {
                labels?.relativeOffset = -x
            }
        }
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    public func didSelect(underlinedSection: UnderlinedHorizontalSectionLabel, index: Int) {
        let oldIndex = currentSectionIndex()
        guard index != oldIndex else { return }
        setViewControllers(
            [section(at: index)].compactMap{ $0 },
            direction: (index > oldIndex ?? index) ? .forward : .reverse,
            animated: oldIndex != nil,
            completion: nil)
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        isTrackingSwipe = true
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        isTrackingSwipe = false
        labels?.select(index: currentSectionIndex(), animated: true)
    }
    
    public private(set) weak var labels: UnderlinedHorizontalSectionLabel?
    private let sectionLabelsBarHeight: CGFloat = 40
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        additionalSafeAreaInsets = UIEdgeInsets(top: sectionLabelsBarHeight, left: 0, bottom: 0, right: 0)
        
        let labels = UnderlinedHorizontalSectionLabel()
        view.addSubview(labels)
        labels.delegate = self
        self.labels = labels
        applyTitlesToLabels()
        labels.layer.zPosition = 1000
        NSLayoutConstraint.activate([
            labels.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -sectionLabelsBarHeight),
            labels.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            labels.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            labels.heightAnchor.constraint(equalToConstant: sectionLabelsBarHeight)
            ])
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(self.swipeTracking(pan:)))
        pan.delegate = self
        view.addGestureRecognizer(pan)
    }
    
    func applyTitlesToLabels() {
        labels?.titles = sections.map { $0.title ?? "" }
        labels?.select(index: currentSectionIndex(), animated: false)
    }
    
}

