//
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


public protocol DragAndPinchController: class {
    
    var dynamicItem: UIDynamicItemWithSettableBounds? { get }
    var dynamicAnimator: UIDynamicAnimator? { get }
    var dragAndPinchBehavior: DragAndPinchBehavior? { get set }
    
    func dynamicItemNaturalSize() -> CGSize
    func dynamicItemMaxScale() -> CGFloat
    func snapBackAnimationSettings() -> AnimationSettings?
    
}


extension DragAndPinchController {
    
    
    public func handleDragAndPinch(_ gesture: DragAndPinchGesture) {
        guard let dynamicItem = dynamicItem else { return }
        guard let view = dynamicAnimator?.referenceView else { return }
        
        if gesture.state == .began {
            if let oldBehavior = dragAndPinchBehavior { dynamicAnimator?.removeBehavior(oldBehavior) }
            dragAndPinchBehavior = DragAndPinchBehavior(item: dynamicItem)
            dynamicAnimator?.addBehavior(dragAndPinchBehavior!)
        }
        
        dragAndPinchBehavior?.updateAnchorPoints(gesture: gesture, referenceView: view)
        dragAndPinchBehavior?.updateSize(gesture: gesture)
        
        if gesture.state == .ended || gesture.state == .cancelled {
            if let oldBehavior = dragAndPinchBehavior { dynamicAnimator?.removeBehavior(oldBehavior) }
            dragAndPinchBehavior = nil
            animateDynamicItemBackAfterGestureEnd()
        }
    }
    
    
    public func animateDynamicItemBackAfterGestureEnd() {
        guard let dynamicItem = dynamicItem else { return }
        guard let view = dynamicAnimator?.referenceView else { return }
        
        let naturalSize = dynamicItemNaturalSize()
        
        let minimumScale = naturalSize.scaleFittingInSize(view.bounds.size) // Fit image in superview
        let maximumScale = minimumScale * dynamicItemMaxScale()
        let currentScale = min(dynamicItem.bounds.size.width / naturalSize.width, dynamicItem.bounds.size.height / naturalSize.height)
        let finalScale = max(minimumScale, min(currentScale, maximumScale))
        
        let bounds = CGRect(origin: CGPoint.zero, size: naturalSize * finalScale)
        
        var center = CGPoint(
            x: max(view.bounds.size.width - 0.5 * bounds.size.width, min(dynamicItem.center.x, 0.5 * bounds.size.width)),
            y: max(view.bounds.size.height - 0.5 * bounds.size.height, min(dynamicItem.center.y, 0.5 * bounds.size.height))
        )
        
        let toleranceForSnappingToCenter: CGFloat = 3.0
        if bounds.size.width < view.bounds.size.width + toleranceForSnappingToCenter { center.x = view.bounds.width * 0.5 }
        if bounds.size.height < view.bounds.size.height + toleranceForSnappingToCenter { center.y = view.bounds.height * 0.5 }
        
        
        UIView.updateUI(snapBackAnimationSettings(), action: { 
            dynamicItem.transform = CGAffineTransform.identity
            dynamicItem.bounds = bounds
            dynamicItem.center = center
        }, completion: nil)
        
    }
    
    
}


func *(lhs: CGSize, rhs: CGFloat) -> CGSize {
    return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
}


extension CGSize {
    
    func scaleFittingInSize(_ boundingSize: CGSize) -> CGFloat {
        return min(boundingSize.width / width, boundingSize.height / height)
    }
    
}
