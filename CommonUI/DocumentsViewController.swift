//
//  DocumentsViewController.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 27/04/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import CommonElements


open class DocumentsViewController: AdaptiveVerticalFlowLayoutCollectionViewController, DocumentsQueryDelegate, UpdatableCollectionController {
    
    
    open var documentCellIdentifier = "DocumentCell"
    
    
    public let documentsQuery = DocumentsQuery()
    
    
    open var items: [Any]?
    
    
    public override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
        setupOnce()
    }
    
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupOnce()
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupOnce()
    }
    
    
    fileprivate func setupOnce() {
        documentsQuery.delegate = self
        documentsQuery.startQuery()
    }
    
    
    open func documentsQueryDidUpdate(_ query: DocumentsQuery, results: NSOrderedSet, removedResults: NSOrderedSet, addedResults: NSOrderedSet, changedResults: NSOrderedSet) {
        
        let oldItems: NSOrderedSet?
        if let items = items {
            oldItems = NSOrderedSet(array: items)
        } else {
            oldItems = nil
        }
        
        update(newItems: results, oldItems: oldItems, removedResults: removedResults, addedResults: addedResults, changedResults: changedResults, section: 0)
        
    }
    
    
    open weak var flow: ListAndItemFlow? {
        willSet {
            flow?.stopObserving(by: self)
        }
        didSet {
            flow?.observe(by: self, queue: .main) { _, _ in
                
            }
        }
    }
    
    
    open override func layoutConfiguration() -> LayoutConfiguration {
        return LayoutConfiguration(
            spacing: 20,
            compactCellHeight: 44,
            regularCellEstimatedWidth: 100,
            regularCellAspectRatio: 1.0)
    }
    
    
    open override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    
    open override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        flow?.selectedItem = items?[indexPath.item]
    }
    
    
    open override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: documentCellIdentifier, for: indexPath)
        
        (cell as? ItemPresenter)?.item = items?[indexPath.item]
        
        return cell
    }
    
    
    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if isViewLoaded && view.window == nil {
            unloadView()
        }
    }
    
    
    open func unloadView() {
        view = nil
    }
    
}
