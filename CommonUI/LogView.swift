//
//  LogView.swift
//
//  Created by Jan Mazurczak on 03/02/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


public enum PlainOrAttributedString {
    case plain(String)
    case attributed(NSAttributedString)
}


public protocol LogEntryInfo {
    var localizedMessage: PlainOrAttributedString { get }
    var backgroundColor: UIColor { get }
}


open class LogView: BlurredRoundContainer<UILabel> {
    
    
    public let label = UILabel(frame: .zero)
    let margin: CGFloat
    
    
    public enum Style {
        case center
        case bottomChat
    }
    
    
    public let style: Style
    
    
    public init(log: LogEntryInfo, style: Style = .center, visualEffect: UIVisualEffect? = UIBlurEffect(style: .light)) {
        self.style = style
        
        let motionForce: CGFloat
        
        switch style {
        case .center:
            margin = 20
            motionForce = 15
        case .bottomChat:
            margin = 5
            motionForce = 8
        }
        
        super.init(view: label, effect: visualEffect, motionEffectForce: motionForce, inset: margin)
        isUserInteractionEnabled = true
        label.isUserInteractionEnabled = false
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LogView.dismiss(by:))))
        
        label.numberOfLines = 0
        backgroundColor = log.backgroundColor
        
        switch log.localizedMessage {
        case .attributed(let text):
            label.attributedText = text
        case .plain(let text):
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .center
            paragraph.lineBreakMode = .byWordWrapping
            paragraph.lineSpacing = 5
            
            let attributes: [NSAttributedString.Key : Any] = [
                .font : UIFont.preferredFont(forTextStyle: .body),
                .foregroundColor : UIColor.white,
                .paragraphStyle : paragraph
            ]
            label.attributedText = NSAttributedString(string: text, attributes: attributes)
        }
        
        maxCornerRadius = margin
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func addConstaraints(in view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        
        switch style {
        case .center:
            _ = addConstraintsToCenterInSuperview()
        case .bottomChat:
            NSLayoutConstraint.activate(NSLayoutConstraint.constraintsWithVisualFormats(
                ["self" : self],
                "V:[self]-\(Int(margin))-|",
                "H:|-\(Int(margin))-[self]"))
        }
        
        let maxWidth: CGFloat
        if view.traitCollection.horizontalSizeClass == .regular {
            maxWidth = (0.3 * view.bounds.size.width) - (2 * margin)
        } else {
            maxWidth = (view.bounds.size.width - 60) - (2 * margin)
        }
        
        let size = label.attributedText?.boundingRect(
            with: CGSize(width: maxWidth, height: view.bounds.size.height - (2 * margin)),
            options: .usesLineFragmentOrigin,
            context: nil) ?? .zero
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(self, .width, ceil(size.width) + (2 * margin), nil, nil),
            NSLayoutConstraint(self, .height, ceil(size.height) + (2 * margin), nil, nil)
            ])
    }
    
    
    public func showBriefly(in view: UIView) {
        
        view.addSubview(self)
        addConstaraints(in: view)
        
        fadeOutByScaling(nil, completion: nil)
        fadeInByScaling(AnimationSettings(duration: 0.5, delay: 0, springDamping: 0.6, springInitial: 0)) { [weak self] _ in
            
            switch self?.style ?? Style.center {
            case .center:
                UIView.updateUI(
                    AnimationSettings(duration: 3, delay: 2, springDamping: 1, springInitial: -1),
                    action:  { [weak self] in
                        self?.effectView.effect = nil
                        self?.label.alpha = 0
                        self?.backgroundColor = .clear
                        self?.transform = self?.transformScaledDownAndMoved(by: CGPoint(x: 0, y: -200)) ?? .identity
                    },
                    completion: { [weak self] _ in self?.removeFromSuperview() }
                )
            case .bottomChat:
                UIView.animate(withDuration: 7.5) {
                    self?.transform = CGAffineTransform(translationX: 0, y: -500)
                }
                UIView.updateUI(
                    AnimationSettings(duration: 1, delay: 6.5, springDamping: 1, springInitial: 0),
                    action:  { [weak self] in
                        self?.effectView.effect = nil
                        self?.label.alpha = 0
                        self?.backgroundColor = .clear
                    },
                    completion: { [weak self] _ in self?.removeFromSuperview() }
                )
                
            }
            
            
            
        }
    }
    
    
    @objc func dismiss(by tap: UITapGestureRecognizer) {
        fadeOutByScaling(AnimationSettings(duration: 0.5, delay: 0, springDamping: 0.6, springInitial: -1)) { _ in
            self.removeFromSuperview()
        }
    }
    
    
}


