//
//  CommonUI.h
//  CommonUI
//
//  Created by Jan Mazurczak on 12/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CommonUI.
FOUNDATION_EXPORT double CommonUIVersionNumber;

//! Project version string for CommonUI.
FOUNDATION_EXPORT const unsigned char CommonUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CommonUI/PublicHeader.h>


