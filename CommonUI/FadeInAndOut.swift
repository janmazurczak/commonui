//
//  FadeInAndOut.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 23/04/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


public protocol FadeInAndOut: class {
    func fadeIn(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?)
    func fadeOut(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?)
}


public protocol FadeInAndOutController {
    func fadeInAndOutChildControllers() -> [FadeInAndOutController]
    func fadeInAndOutAwareItems() -> [FadeInAndOut]
    func prepareToFadeIn()
    func fadeIn(initialDelay: TimeInterval, duration: TimeInterval, completion: ((Bool) -> Void)?)
    func fadeOut(initialDelay: TimeInterval, duration: TimeInterval, completion: ((Bool) -> Void)?)
}


extension FadeInAndOutController {
    
    
    public func prepareToFadeIn() {
        fadeInAndOutChildControllers().forEach { $0.prepareToFadeIn() }
        fadeInAndOutAwareItems().forEach { $0.fadeOut(nil, completion: nil) }
    }
    
    
    public func fadeIn(initialDelay: TimeInterval, duration: TimeInterval, completion: ((Bool) -> Void)?) {
        fadeInAndOutChildControllers().forEach { $0.fadeIn(initialDelay: initialDelay, duration: duration, completion: completion) }
        
        let items = fadeInAndOutAwareItems()
        if items.count == 0 { completion?(true); return }
        
        enumerateFadeInAndOutItemsIncreasingDelay(items, initialDelay: initialDelay, duration: duration, springDamping: 0.6, springInitial: 0) { item, animationSettings, isLast in
            
            item.fadeIn(animationSettings, completion: isLast ? completion : nil)
        }
        
    }
    
    
    public func fadeOut(initialDelay: TimeInterval, duration: TimeInterval, completion: ((Bool) -> Void)?) {
        fadeInAndOutChildControllers().forEach { $0.fadeOut(initialDelay: initialDelay, duration: duration, completion: completion) }
        
        let items = fadeInAndOutAwareItems()
        if items.count == 0 { completion?(true); return }
        
        enumerateFadeInAndOutItemsIncreasingDelay(items.reversed(), initialDelay: initialDelay, duration: duration, springDamping: 0.8, springInitial: -1) { item, animationSettings, isLast in
            
            item.fadeOut(animationSettings, completion: isLast ? completion : nil)
            }
        
    }
    
    
    fileprivate func enumerateFadeInAndOutItemsIncreasingDelay(_ items: [FadeInAndOut], initialDelay: TimeInterval, duration: TimeInterval, springDamping: CGFloat, springInitial: CGFloat, action: ((_ item: FadeInAndOut, _ animationSettings: AnimationSettings, _ isLast: Bool) -> Void)) {
        
        let singleDuration = duration / (TimeInterval(items.count) * 0.5 + 0.5)
        let deltaDelay = 0.5 * singleDuration
        
        var animationSettings = AnimationSettings(duration: singleDuration, delay: initialDelay, springDamping: springDamping, springInitial: springInitial)
        
        var i = 0
        items.forEach {
            i += 1
            action($0, animationSettings, i == items.count)
            animationSettings.delay += deltaDelay
        }
    }
    
    
}


extension UIView {
    
    
    public func fadeInByResettingTransform(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        UIView.updateUI(animationSettings, action: {
            self.transform = CGAffineTransform.identity
        }, completion: completion)
    }
    
    
    public func fadeInByScaling(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        fadeInByResettingTransform(animationSettings, completion: completion)
    }
    
    
    public func fadeOutByScaling(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        UIView.updateUI(animationSettings, action: {
            self.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
        }, completion: completion)
    }
    
    
    public func fadeInBySliding(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        fadeInByResettingTransform(animationSettings, completion: completion)
    }
    
    
    public func fadeOutBySliding(_ animationSettings: AnimationSettings?, by offset: CGPoint, completion: ((Bool) -> Void)?) {
        UIView.updateUI(animationSettings, action: {
            self.transform = CGAffineTransform(translationX: offset.x, y: offset.y)
        }, completion: completion)
    }
    
    
    public func fadeInByDimming(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        var settings = animationSettings
        settings?.springDamping = 1
        UIView.updateUI(settings, action: {
            self.alpha = 1
        }, completion: completion)
    }
    
    
    public func fadeOutByDimming(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        var settings = animationSettings
        settings?.springDamping = 1
        UIView.updateUI(settings, action: {
            self.alpha = 0
        }, completion: completion)
    }
    
    
    public func fadeInLikeMistyGhost(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        var settings = animationSettings
        settings?.springDamping = 1
        UIView.updateUI(settings, action: {
            self.alpha = 1
            self.transform = CGAffineTransform.identity
        }, completion: completion)
    }
    
    
    public func fadeOutLikeMistyGhost(_ animationSettings: AnimationSettings?, moveBy: CGPoint, completion: ((Bool) -> Void)?) {
        var settings = animationSettings
        settings?.springDamping = 1
        UIView.updateUI(settings, action:  {
            self.alpha = 0
            self.transform = self.transformScaledDownAndMoved(by: moveBy)
        }, completion: completion)
    }
    
    
    public func transformScaledDownAndMoved(by offset: CGPoint) -> CGAffineTransform {
        return CGAffineTransform(scaleX: 0.5, y: 0.5).translatedBy(x: offset.x, y: offset.y)
    }
    
    
    public func offsetSlightlyMovingAwayFromSuperviewCenter() -> CGPoint {
        let centerDelta: CGPoint
        if let superview = superview {
            centerDelta = CGPoint(
                x: center.x - superview.bounds.midX,
                y: center.y - superview.bounds.midY
            )
        } else {
            centerDelta = CGPoint.zero
        }
        
        let maximumOffset: CGFloat = 50.0
        
        let offset = CGPoint(
            x: max(-maximumOffset, min(centerDelta.x * 0.2, maximumOffset)),
            y: max(-maximumOffset, min(centerDelta.y * 0.2, maximumOffset))
        )
        return offset
    }
    
    
}

