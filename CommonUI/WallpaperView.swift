//
//  Created by Jan Mazurczak on 18/04/16.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class ReplicatorView<ContentViewType: UIView>: UIView {
    
    open override class var layerClass: AnyClass {
        return CAReplicatorLayer.self
    }
    
    open var replicatorLayer: CAReplicatorLayer { return layer as! CAReplicatorLayer }
    
    public let contentView = ContentViewType(frame: CGRect.zero)
    
    open var contentViewSize: CGSize {
        let size = contentView.intrinsicContentSize
        return CGSize(width: max(1, size.width), height: max(1, size.height))
    }
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupOnce()
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupOnce()
    }
    
    
    open func setupOnce() {
        addSubview(contentView)
    }
    
}


open class WallpaperView<ContentViewType: UIView>: ReplicatorView<WallpaperVerticalSliceView<ContentViewType>> {
    
    public override init(frame: CGRect) { super.init(frame: frame) }
    public required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    
    open var tileContentView: ContentViewType { return contentView.contentView }
    open var tileContentViewSize: CGSize { return contentView.contentViewSize }
    
    open override func setupOnce() {
        super.setupOnce()
        contentView.addConstraintsToFillSuperview()
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        tileContentView.transform = CGAffineTransform(
            translationX: offsetToCenter(small: tileContentViewSize.width, in: bounds.size.width),
            y: offsetToCenter(small: tileContentViewSize.height, in: bounds.size.height))
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(tileContentViewSize.width, -0.5 * tileContentViewSize.height, 0)
        replicatorLayer.instanceCount = Int(bounds.size.width / max(1, tileContentViewSize.width)) + 1
    }
    
    private func offsetToCenter(small: CGFloat, in big: CGFloat) -> CGFloat {
        return (big - small - (floor(big / small) * small)) * 0.5
    }
    
}


public class WallpaperVerticalSliceView<ContentViewType: UIView>: ReplicatorView<ContentViewType> {
    
    public override func setupOnce() {
        super.setupOnce()
        contentView.addConstraintsToAlignTopLeft()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(0, contentViewSize.height, 0)
        let xCount = floor((superview?.bounds.size.width ?? 0) / contentViewSize.width) + 1
        replicatorLayer.instanceCount = Int(
            ((xCount * 0.5 * contentViewSize.height) + bounds.size.height)
                / max(1, contentViewSize.height)) + 1
    }
    
}
