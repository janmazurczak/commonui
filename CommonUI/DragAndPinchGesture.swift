//
//  Copyright (c) 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import UIKit.UIGestureRecognizerSubclass


open class DragAndPinchGesture: UIGestureRecognizer {
    
    
    open fileprivate(set) var scale: CGFloat? = nil
    fileprivate var pinchDistance: CGFloat = 0.0
    fileprivate var pinchDistanceAtBeginning: CGFloat? = nil
    
    
    func resetScale() {
        scale = nil
        pinchDistanceAtBeginning = pinchDistance
    }
    
    
    func processPinch() {
        if numberOfTouches == 2 {
            let posA = location(ofTouch: 0, in: view)
            let posB = location(ofTouch: 1, in: view)
            let x = posA.x - posB.x
            let y = posA.y - posB.y
            pinchDistance = sqrt(x * x + y * y)
            if let pinchDistanceAtBeginning = pinchDistanceAtBeginning {
                scale = pinchDistance / fmax(1.0, pinchDistanceAtBeginning)
            } else {
                pinchDistanceAtBeginning = pinchDistance
                scale = 1.0
            }
        } else {
            pinchDistanceAtBeginning = nil
            scale = nil
        }
    }
    
    
    func allTouchesPoints(_ view: UIView) -> [CGPoint] {
        var result: [CGPoint] = []
        let touchesCount = numberOfTouches
        for i in 0 ..< touchesCount {
            let touchPoint = location(ofTouch: i, in: view)
            result.append(touchPoint)
        }
        return result
    }
    
    
    var gestureStartedTrackingTimestamp: TimeInterval? = nil
    var gestureRecognizedAndBegan: Bool = false
    open var delayRecognizing: TimeInterval? = 0.05
    
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        if (state == .possible) && (gestureStartedTrackingTimestamp == nil) && (numberOfTouches - touches.count == 0) {
            if let delay = delayRecognizing, touches.count == 1 {
                let timestamp = touches.first!.timestamp
                let time = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(delay * 1000))
                gestureStartedTrackingTimestamp = timestamp
                weak var weakSelf = self
                DispatchQueue.main.asyncAfter(deadline: time, execute: {
                    if let gesture = weakSelf {
                        if timestamp == gesture.gestureStartedTrackingTimestamp {
                            gesture.gestureStartedTrackingTimestamp = nil
                            gesture.gestureRecognizedAndBegan = true
                        }
                    }
                })
            } else {
                gestureStartedTrackingTimestamp = nil
                gestureRecognizedAndBegan = true
            }
        }
    }
    
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        if gestureRecognizedAndBegan {
            if state == .possible {
                state = .began
            } else {
                processPinch()
                state = .changed
            }
        }
    }
    
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        if gestureRecognizedAndBegan && state != .possible {
            processPinch()
            if numberOfTouches - touches.count == 0 {
                state = .ended
            } else {
                state = .changed
            }
        } else {
            state = .failed
        }
        
    }
    
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesCancelled(touches, with: event)
        if gestureRecognizedAndBegan && state != .possible {
            processPinch()
            if numberOfTouches - touches.count == 0 {
                state = .cancelled
            } else {
                state = .changed
            }
        } else {
            state = .failed
        }
    }
    
    
    open override func reset() {
        super.reset()
        if state == .ended || state == .failed {
            gestureStartedTrackingTimestamp = nil
            gestureRecognizedAndBegan = false
            pinchDistanceAtBeginning = nil
            scale = nil
        }
    }
    
}
