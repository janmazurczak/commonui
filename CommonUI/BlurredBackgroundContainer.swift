//
//  BlurredBackgroundViewController.swift
//  Grain
//
//  Created by Jan Mazurczak on 05/07/15.
//  Copyright © 2015 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class BlurredBackgroundContainer<RootVC>: UIViewController, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning where RootVC: UIViewController, RootVC: FadeInAndOutController {
    
    
    public let blurEffectStyle: UIBlurEffect.Style
    public let rootViewController: RootVC
    
    
    public init(rootViewController: RootVC, style: UIBlurEffect.Style) {
        self.rootViewController = rootViewController
        self.blurEffectStyle = style
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
        
        addChild(rootViewController)
        rootViewController.didMove(toParent: self)
    }
    
    
    public required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    open override func loadView() {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: blurEffectStyle))
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.contentView.addSubview(rootViewController.view)
        rootViewController.view.translatesAutoresizingMaskIntoConstraints = false
        rootViewController.view.addConstraintsToFillSuperview()
        
        self.view = view
    }
    
    
    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if isViewLoaded && view.window == nil {
            unloadView()
        }
    }
    
    
    open func unloadView() {
        view = nil
    }
    
    
    open var isBackgroundBlurred: Bool = false {
        didSet {
            (view as! UIVisualEffectView).effect = isBackgroundBlurred ? UIBlurEffect(style: blurEffectStyle) : nil
        }
    }
    
    
    public var contentView: UIView {
        return (view as! UIVisualEffectView).contentView
    }
    
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.7
    }
    
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let dismissing = transitionContext.viewController(forKey: .from) == self
        
        animatedTransitionPrepare(containerView: transitionContext.containerView, dismissing: dismissing)
        
        let duration = transitionDuration(using: transitionContext)
        
        if dismissing {
            rootViewController.fadeOut(initialDelay: 0, duration: duration, completion: nil)
        } else {
            rootViewController.fadeIn(initialDelay: 0, duration: duration, completion: nil)
        }
        
        UIView.animate(withDuration: duration, animations:
            {
                self.animatedTransition(dismissing: dismissing)
        }, completion: {
            finish in
            self.animatedTransitionEnd(dismissing: dismissing)
            transitionContext.completeTransition(finish)
        })
    }
    
    
    func animatedTransitionPrepare(containerView: UIView, dismissing: Bool) {
        if !dismissing {
            (view as? UIVisualEffectView)?.effect = nil
            containerView.addSubview(view)
            view.addConstraintsToFillSuperview()
            rootViewController.prepareToFadeIn()
        }
    }
    
    
    func animatedTransition(dismissing: Bool) {
        if dismissing {
            isBackgroundBlurred = false
        } else {
            isBackgroundBlurred = true
        }
    }
    
    
    func animatedTransitionEnd(dismissing: Bool) {
        if dismissing {
            view.removeFromSuperview()
        }
    }
    
    
}

