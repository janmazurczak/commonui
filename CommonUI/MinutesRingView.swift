//
//  MinutesRingView.swift
//  Amama
//
//  Created by Jan Mazurczak on 13/07/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class MinutesRingView: UIView {
    
    
    private let numberOfMinutes = 60
    private let minuteHeight: CGFloat = 8
    private let minuteWidth: CGFloat = 1
    
    
    open override class var layerClass: AnyClass { return CAReplicatorLayer.self }
    private var replicatorLayer: CAReplicatorLayer { return self.layer as! CAReplicatorLayer }
    private let minuteLayer = CAShapeLayer()
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupOnce()
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupOnce()
    }
    
    
    open var progress: Float = 0 {
        didSet {
            let animKey = "progress"
            let finalCount = max(1, min(Int(ceil(progress * Float(numberOfMinutes))), numberOfMinutes))
            if finalCount == replicatorLayer.instanceCount { return }
            
            let anim = CABasicAnimation(keyPath: "instanceCount")
            anim.fromValue = replicatorLayer.presentation()?.instanceCount ?? replicatorLayer.instanceCount
            anim.toValue = finalCount
            anim.duration = 0.1
            
            replicatorLayer.removeAnimation(forKey: animKey)
            replicatorLayer.add(anim, forKey: animKey)
            replicatorLayer.instanceCount = finalCount
        }
    }
    
    
    open var presented: Bool = false {
        didSet {
            if presented == oldValue { return }
            
            let animKey = "presenting"
            let finalPath = minuteShape(presented: presented)
            
            let anim = CABasicAnimation(keyPath: "path")
            anim.fromValue = minuteLayer.presentation()?.path ?? minuteShape(presented: !presented)
            anim.toValue = finalPath
            anim.duration = presented ? 0.2 : 0.6
            
            minuteLayer.removeAnimation(forKey: animKey)
            minuteLayer.add(anim, forKey: animKey)
            minuteLayer.path = finalPath
        }
    }
    
    
    private func setupOnce() {
        clipsToBounds = false
        
        replicatorLayer.instanceCount = 1
        replicatorLayer.instanceTransform = CATransform3DMakeRotation((.pi * 2) / CGFloat(numberOfMinutes), 0, 0, 1)
        
        minuteLayer.fillColor = UIColor.clear.cgColor
        minuteLayer.lineCap = CAShapeLayerLineCap.round
        minuteLayer.lineJoin = CAShapeLayerLineJoin.round
        minuteLayer.lineWidth = minuteWidth
        minuteLayer.strokeColor = UIColor.black.cgColor
        
        replicatorLayer.addSublayer(minuteLayer)
    }
    
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        minuteLayer.frame = replicatorLayer.bounds
        minuteLayer.path = minuteShape(presented: presented)
    }
    
    
    private func minuteShape(presented: Bool) -> CGPath {
        let minuteShape = UIBezierPath()
        minuteShape.move(to: CGPoint(x: minuteLayer.bounds.midX, y: presented ? 0 : minuteHeight * 0.5))
        minuteShape.addLine(to: CGPoint(x: minuteLayer.bounds.midX, y: presented ? minuteHeight : minuteHeight * 0.5))
        return minuteShape.cgPath
    }
    
    
}
