//
//  RecordButton.swift
//  Amama
//
//  Created by Jan Mazurczak on 11/07/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class RecordButton: UIControl {
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupOnce()
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupOnce()
    }
    
    
    private func setupOnce() {
        backgroundColor = .clear
        circleView.backgroundColor = UIColor(red:0.95, green:0.21, blue:0.23, alpha:1.00)
        circleView.isUserInteractionEnabled = false
        addSubview(circleView)
        addSubview(roundLabel)
    }
    
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        ringView?.frame = bounds
        let circleFrame = bounds.insetBy(dx: 12, dy: 12)
        circleView.frame = circleFrame
        roundLabel.frame = circleFrame
        circleView.layer.cornerRadius = 0.5 * min(circleView.bounds.size.width, circleView.bounds.size.height)
    }
    
    
    open func showProgress() {
        ringView = MinutesRingView(frame: bounds)
    }
    
    
    open func hideProgress() {
        ringView = nil
    }
    
    
    open var progress: Float {
        set { ringView?.progress = newValue }
        get { return ringView?.progress ?? 0 }
    }
    
    
    private var ringView: MinutesRingView? {
        willSet {
            if let ringView = ringView {
                ringView.presented = false
                UIView.animate(
                    withDuration: 1.2,
                    animations: { ringView.alpha = 0 },
                    completion: { _ in ringView.removeFromSuperview() })
            }
        }
        didSet {
            if let ringView = ringView {
                ringView.isUserInteractionEnabled = false
                ringView.frame = bounds
                insertSubview(ringView, belowSubview: circleView)
                ringView.setNeedsLayout()
                ringView.layoutIfNeeded()
                ringView.presented = true
            }
        }
    }
    
    
    public let circleView = UIView(frame: CGRect.zero)
    public let roundLabel = RoundLabel(frame: CGRect.zero)
    
    
    open override var isHighlighted: Bool {
        didSet {
            if isHighlighted == oldValue { return }
            
            let animKey = "highlighting"
            let keyPath = "transform.scale"
            let anim = CAKeyframeAnimation(keyPath: keyPath)
            if isHighlighted {
                let timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                anim.values = [1.0, 0.95, 1.0]
                anim.timingFunctions = [timingFunction, timingFunction]
                anim.repeatCount = .infinity
                anim.duration = 1.0
            } else {
                let timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
                let currentValue = circleView.layer.presentation()?.value(forKeyPath: keyPath)
                anim.values = [currentValue ?? 1.0, 1.15, 1.0]
                anim.timingFunctions = [timingFunction, timingFunction]
                anim.duration = 0.5
            }
            
            circleView.layer.removeAnimation(forKey: animKey)
            circleView.layer.add(anim, forKey: animKey)
        }
    }
    
    
}
