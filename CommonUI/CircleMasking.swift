//
//  Created by Jan Mazurczak on 14/04/16.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


public protocol PathMaskApplicable: class {
    var layer: CALayer { get }
    var bounds: CGRect { get }
    
    var maskBorderLayer: CAShapeLayer? { get set }
}


extension PathMaskApplicable {
    
    
    public var pathMaskLayer: CAShapeLayer? { return layer.mask as? CAShapeLayer }
    
    
    public func addMask(border: (width: CGFloat, color: UIColor)?) {
        layer.mask = CAShapeLayer()
        if let border = border {
            let borderLayer = CAShapeLayer()
            borderLayer.lineWidth = border.width * 2.0 // half width will be masked, so *2 makes it as thick as user wants with border argument
            borderLayer.strokeColor = border.color.cgColor
            borderLayer.fillColor = UIColor.clear.cgColor
            layer.addSublayer(borderLayer)
            maskBorderLayer = borderLayer
        }
    }
    
    
    public func setMaskPath(_ path: UIBezierPath, animationSettings: AnimationSettings?) {
        
        if let animationSettings = animationSettings {
            setMaskPathWithAnimation(path, animationSettings: animationSettings)
        } else {
            setMaskPathWithoutAnimation(path)
        }
        
    }
    
    
    fileprivate func setMaskPathWithoutAnimation(_ path: UIBezierPath) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        [pathMaskLayer, maskBorderLayer].forEach {
            $0?.path = path.cgPath
        }
        CATransaction.commit()
    }
    
    
    fileprivate func setMaskPathWithAnimation(_ path: UIBezierPath, animationSettings: AnimationSettings) {
        let anim = CAKeyframeAnimation(keyPath: "path")
        anim.values = [
            pathMaskLayer?.presentation()?.path ?? path.cgPath,
            path.cgPath
        ]
        anim.beginTime = CACurrentMediaTime() + animationSettings.delay
        anim.duration = animationSettings.duration
        anim.fillMode = CAMediaTimingFillMode.backwards
        anim.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        [pathMaskLayer, maskBorderLayer].forEach {
            $0?.removeAnimation(forKey: "pathChange")
            $0?.add(anim, forKey: "pathChange")
            $0?.path = path.cgPath
        }
    }
    
    
}


public protocol CircleMaskApplicable: PathMaskApplicable {
    
}


extension CircleMaskApplicable {
    
    
    public func adjustCircleMaskToBounds(animationSettings: AnimationSettings?) {
        setMaskPath(UIBezierPath(ovalIn: bounds), animationSettings: animationSettings)
    }
    
    
    public func collapseCircleMask(animationSettings: AnimationSettings?) {
        let collapsedRect = CGRect(origin: CGPoint(x: bounds.midX, y: bounds.midY), size: CGSize.zero)
        setMaskPath(UIBezierPath(ovalIn: collapsedRect), animationSettings: animationSettings)
    }
    
    
}
