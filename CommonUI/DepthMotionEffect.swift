//
//  Created by Jan Mazurczak on 14/04/16.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class DepthMotionEffect: UIMotionEffect {
    
    
    open var force: CGFloat = 10.0
    public let positionKeyPath: String
    
    
    public init(force: CGFloat, positionKeyPath: String = "center") {
        self.positionKeyPath = positionKeyPath
        super.init()
        self.force = force
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        self.positionKeyPath = "center"
        super.init(coder: aDecoder)
    }
    
    
    open override func keyPathsAndRelativeValues(forViewerOffset viewerOffset: UIOffset) -> [String : Any]? {
        let point = CGPoint(x: viewerOffset.horizontal * force, y: viewerOffset.vertical * force)
        return [positionKeyPath : NSValue(cgPoint: point)]
    }
    
    
}
