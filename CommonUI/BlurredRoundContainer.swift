//
//  Created by Jan Mazurczak on 30/01/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class BlurredRoundContainer<ChildView>: UIView where ChildView: UIView {
    
    
    public let effectView = UIVisualEffectView(effect: nil)
    
    
    public init(view: ChildView, effect: UIVisualEffect?, motionEffectForce: CGFloat?, inset: CGFloat) {
        super.init(frame: .zero)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        effectView.effect = effect
        effectView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(effectView)
        effectView.addConstraintsToFillSuperview()
        
        effectView.contentView.addSubview(view)
        view.addConstraintsToFillSuperview(with: inset)
        
        clipsToBounds = true
        
        if let motionEffectForce = motionEffectForce {
            motionEffects = [DepthMotionEffect(force: motionEffectForce)]
        }
    }
    
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    open var maxCornerRadius: CGFloat? { didSet { setNeedsLayout() } }
    
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        if let maxCornerRadius = maxCornerRadius {
            layer.cornerRadius = min(0.5 * min(bounds.size.width, bounds.size.height), maxCornerRadius)
        } else {
            layer.cornerRadius = 0.5 * min(bounds.size.width, bounds.size.height)
        }
        
    }
    
    
}
