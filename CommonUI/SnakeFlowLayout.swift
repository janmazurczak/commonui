//
//  SnakeFlowLayout.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 24/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.



/*
 
 Snake Flow Layout in current implementation requires:
 - constant itemSize
 - only one section in collection view
 
 vertical scrolling cells scheme:
 123
 654
 789
 
 horizontal scrolling cells scheme:
 167
 258
 349
 
 */

import UIKit


open class SnakeFlowLayoutAttributes: UICollectionViewLayoutAttributes {
    
    
    public enum Direction {
        case left, up, right, down
        func opposite() -> Direction {
            switch self {
            case .left: return .right
            case .right: return .left
            case .up: return .down
            case .down: return .up
            }
        }
    }
    
    
    open var previousItemDirection: Direction?
    open var nextItemDirection: Direction?
    
    
    open override func isEqual(_ object: Any?) -> Bool {
        if let
            attributes = object as? SnakeFlowLayoutAttributes ,
            attributes.nextItemDirection == nextItemDirection &&
            attributes.previousItemDirection == previousItemDirection
        {
            return super.isEqual(object)
        } else {
            return false
        }
    }
    
    
    open override func copy(with zone: NSZone?) -> Any {
        let attributes = super.copy(with: zone)
        if let attributes = attributes as? SnakeFlowLayoutAttributes {
            attributes.nextItemDirection = nextItemDirection
            attributes.previousItemDirection = previousItemDirection
        }
        return attributes
    }
    
    
}


open class SnakeFlowLayout: UICollectionViewFlowLayout {
    
    
    open override class var layoutAttributesClass: AnyClass {
        return SnakeFlowLayoutAttributes.self
    }
    
    
    open override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let superResults = super.layoutAttributesForElements(in: rect)
        var results = [UICollectionViewLayoutAttributes]()
        superResults?.forEach {
            if let attributes = $0.copy() as? SnakeFlowLayoutAttributes {
                applySnakeAttributes(attributes)
                results.append(attributes)
            }
        }
        return results
    }
    
    
    open override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if let attributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? SnakeFlowLayoutAttributes {
            applySnakeAttributes(attributes)
            return attributes
        } else {
            return super.layoutAttributesForItem(at: indexPath)
        }
    }
    
    
    open override func prepare() {
        super.prepare()
        
        spaceAvailableInLine = collectionViewContentSize.nonScrollableSize(scrollDirection) - sectionInset.nonScrollableSize(scrollDirection)
        numberOfItemsInLine = max(1, Int(floor(
            (spaceAvailableInLine + minimumInteritemSpacing) / (itemSize.nonScrollableSize(scrollDirection) + minimumInteritemSpacing)
            )))
    }
    
    
    fileprivate var spaceAvailableInLine: CGFloat = 0
    fileprivate(set) var numberOfItemsInLine: Int = 1
    
    
    fileprivate func lineAtIndex(_ index: Int) -> Int {
        return index / numberOfItemsInLine
    }
    
    
    fileprivate func isIndexValid(_ index: Int) -> Bool {
        return index >= 0 && index < (collectionView?.numberOfItems(inSection: 0) ?? 0)
    }
    
    
    fileprivate func nextItemDirectionAtIndex(_ index: Int) -> SnakeFlowLayoutAttributes.Direction? {
        let nextItemIndex = index + 1
        guard isIndexValid(nextItemIndex) else { return nil }
        
        let line = lineAtIndex(index)
        let reversed = line % 2 == 1
        
        if lineAtIndex(nextItemIndex) > line {
            return .down
        } else if reversed {
            return .left
        } else {
            return .right
        }
    }
    
    
    fileprivate func previousItemDirectionAtIndex(_ index: Int) -> SnakeFlowLayoutAttributes.Direction? {
        let previousItemIndex = index - 1
        guard isIndexValid(previousItemIndex) else { return nil }
        return nextItemDirectionAtIndex(previousItemIndex)?.opposite()
    }
    
    
    fileprivate func applySnakeAttributes(_ attributes: SnakeFlowLayoutAttributes) {
        let index = attributes.indexPath.item
        let line = lineAtIndex(index)
        
        attributes.nextItemDirection = nextItemDirectionAtIndex(index)
        attributes.previousItemDirection = previousItemDirectionAtIndex(index)
        
        let reversed = line % 2 == 1
        
        if reversed {
            reversePosition(attributes)
        }
    }
    
    
    fileprivate func reversePosition(_ attributes: SnakeFlowLayoutAttributes) {
        attributes.center.setNonScrollableAxisValue(
            (2 * sectionInset.nonScrollableStartMargin(scrollDirection)) +
                spaceAvailableInLine - attributes.center.nonScrollableAxisValue(scrollDirection),
            scrollDirection: scrollDirection)
    }
    
    
}
