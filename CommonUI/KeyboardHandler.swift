//
//  KeyboardHandler.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 05/09/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import Foundation
import UIKit


public protocol KeyboardHandler: class {
    var bottomMarginConstraint: NSLayoutConstraint? { get }
    var bottomMarginAboveKeyboard: CGFloat { get }
    var contentView: UIView { get }
    var keyboardWatcher: KeyboardWatcher? { get set }
    
    func watchKeyboardChanges()
    func unwatchKeyboardChanges()
    func handleKeyboardFrameChange(to keyboardFrameInScreen: CGRect, duration: TimeInterval?, curve: UInt?)
}


extension KeyboardHandler {
    
    
    public func watchKeyboardChanges() {
        keyboardWatcher = KeyboardWatcher()
        keyboardWatcher?.handler = self
        keyboardWatcher?.watchKeyboardChanges()
    }
    
    
    public func unwatchKeyboardChanges() {
        keyboardWatcher?.unwatchKeyboardChanges()
        keyboardWatcher?.handler = nil
        keyboardWatcher = nil
    }
    
    
    public func handleKeyboardFrameChange(to keyboardFrameInScreen: CGRect, duration: TimeInterval?, curve: UInt?) {
        
        let keyboardFrame = contentView.convert(keyboardFrameInScreen, from: nil)
        
        bottomMarginConstraint?.constant = max(0, contentView.bounds.size.height - keyboardFrame.origin.y) + bottomMarginAboveKeyboard
        
        UIView.animate(
            withDuration: duration ?? 0.5,
            delay: 0,
            options: UIView.AnimationOptions(rawValue: curve ?? UIView.AnimationOptions.curveEaseInOut.rawValue),
            animations: { self.contentView.layoutIfNeeded() },
            completion: { _ in
                
        })
    }
    
    
}


public class KeyboardWatcher {
    
    
    weak var handler: KeyboardHandler?
    
    
    func watchKeyboardChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardWatcher.keyboardFrameWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    
    func unwatchKeyboardChanges() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    
    @objc func keyboardFrameWillChange(notification: NSNotification) {
        
        guard let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let duration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue
        let curve = (notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
        
        handler?.handleKeyboardFrameChange(to: keyboardFrame, duration: duration, curve: curve)
    }
    
    
}

