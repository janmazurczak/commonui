//
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import QuartzCore


public struct AnimationSettings {
    
    public var duration: TimeInterval
    public var delay: TimeInterval
    public var springDamping: CGFloat
    public var springInitial: CGFloat
    
    
    public init(duration: TimeInterval, delay: TimeInterval, springDamping: CGFloat, springInitial: CGFloat) {
        self.duration = duration
        self.delay = delay
        self.springDamping = springDamping
        self.springInitial = springInitial
    }
    
}


public extension AnimationSettings {
    
    
    static func +(lhs: AnimationSettings, rhs: AnimationSettings) -> AnimationSettings {
        return AnimationSettings(duration: lhs.duration + rhs.duration, delay: lhs.delay + rhs.delay, springDamping: lhs.springDamping + rhs.springDamping, springInitial: lhs.springInitial + rhs.springInitial)
    }
    
    
    func adding(rhs: AnimationSettings) -> AnimationSettings {
        return self + rhs
    }
    
}


extension UIView {
    
    public class func updateUI(_ animationSettings: AnimationSettings?, action: @escaping () -> Void, completion: ((Bool) -> Void)?) {
        if let settings = animationSettings {
            self.animate(with: settings, animations: action, completion: completion)
        } else {
            action()
            completion?(true)
        }
    }
    
    
    public class func animate(with settings: AnimationSettings = AnimationSettings(duration: 0.5, delay: 0, springDamping: 0.7, springInitial: 0), animations: @escaping () -> Void, completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: settings.duration, delay: settings.delay, usingSpringWithDamping: settings.springDamping, initialSpringVelocity: settings.springInitial, options: [.beginFromCurrentState, .allowUserInteraction], animations: animations, completion: completion)
    }
    
}


extension CAAnimation {
    
    public class func spinningAnimation(_ pointKey: String, radius: CGFloat, spinDuration: CFTimeInterval) -> CAAnimation {
        let timingFunctionEaseInOut = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        let animH = CAKeyframeAnimation(keyPath: "\(pointKey).x")
        animH.values = [-radius, radius, -radius]
        animH.timeOffset = spinDuration * 0.25
        animH.timingFunctions = [
            timingFunctionEaseInOut,
            timingFunctionEaseInOut
        ]
        animH.isAdditive = true
        
        let animV = CAKeyframeAnimation(keyPath: "\(pointKey).y")
        animV.values = [-radius, radius, -radius]
        animV.timingFunctions = [
            timingFunctionEaseInOut,
            timingFunctionEaseInOut
        ]
        animV.isAdditive = true
        
        let anim = CAAnimationGroup()
        anim.animations = [animH, animV]
        anim.duration = spinDuration
        anim.repeatCount = Float.infinity
        
        return anim
    }
    
}
