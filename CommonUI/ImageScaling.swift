//
//  ImageScaling.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 02/12/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import CoreImage


public extension UIImage {
    
    
    func scaledDownIfNeeded(to maxSize: CGSize, applyingFilters: [(name: String, parameters: [String : Any])] = []) -> UIImage? {
        let demandedScale = min(maxSize.width / size.width, maxSize.height / size.height)
        if demandedScale < 1 {
            
            guard var ciImage = CIImage(image: self) else { return nil }
            
            ciImage = ciImage.transformed(by: CGAffineTransform(scaleX: demandedScale, y: demandedScale))
            
            for filter in applyingFilters {
                ciImage = ciImage.applyingFilter(filter.name, parameters: filter.parameters)
            }
            
            guard let cgImage = CIContext(options: nil).createCGImage(ciImage, from: ciImage.extent) else { return nil }
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        } else {
            return self
        }
    }
    
    
}

