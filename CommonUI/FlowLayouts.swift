//
//  FlowLayouts.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 12/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


extension UICollectionViewController {
    
    public var flowLayout: UICollectionViewFlowLayout? {
        return collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    }
    
}


extension UICollectionViewFlowLayout {
    
    public func setupAsStandardVerticalLayout(spacing: CGFloat = 4.0, sideMargin: CGFloat = 0.0) {
        scrollDirection = .vertical
        minimumInteritemSpacing = spacing
        minimumLineSpacing = spacing
        sectionInset = UIEdgeInsets(top: spacing, left: sideMargin, bottom: spacing, right: sideMargin)
    }
    
}


open class FullWidthAutosizingCell: UICollectionViewCell {
    
    open override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size, withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
        
        let result = super.preferredLayoutAttributesFitting(layoutAttributes)
        result.size = CGSize(width: layoutAttributes.size.width, height: size.height)
        
        return result
    }
    
}


extension CGSize {
    
    
    public func nonScrollableSize(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? width : height
    }
    
    
    public func scrollableSize(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? height : width
    }
    
    
}


extension CGPoint {
    
    
    public func nonScrollableAxisValue(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? x : y
    }
    
    
    public func scrollableAxisValue(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? y : x
    }
    
    
    public mutating func setNonScrollableAxisValue(_ value: CGFloat, scrollDirection: UICollectionView.ScrollDirection) {
        if scrollDirection == .vertical {
            x = value
        } else {
            y = value
        }
    }
    
    
    public mutating func setScrollableAxisValue(_ value: CGFloat, scrollDirection: UICollectionView.ScrollDirection) {
        if scrollDirection == .vertical {
            y = value
        } else {
            x = value
        }
    }
    
    
}


extension UIEdgeInsets {
    
    
    public func nonScrollableStartMargin(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? left : top
    }
    
    
    public func scrollableStartMargin(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? top : left
    }
    
    
    public func nonScrollableEndMargin(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? right : bottom
    }
    
    
    public func scrollableEndMargin(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollDirection == .vertical ? bottom : right
    }
    
    
    public func nonScrollableSize(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return nonScrollableStartMargin(scrollDirection) + nonScrollableEndMargin(scrollDirection)
    }
    
    
    public func scrollableSize(_ scrollDirection: UICollectionView.ScrollDirection) -> CGFloat {
        return scrollableStartMargin(scrollDirection) + scrollableEndMargin(scrollDirection)
    }
    
    
}



