//
//  InterfaceOrientation.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 02/04/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


public protocol UIInterfaceOrientationObserver: class {
    var currentInterfaceOrientation: UIInterfaceOrientation { get set }
    var supportedInterfaceOrientations: UIInterfaceOrientationMask { get }
    var shouldAutorotate: Bool { get }
    func setupInterfaceOrientation()
    func startObservingUIOrientation()
    func stopObservingUIOrientation()
}


public extension UIInterfaceOrientationObserver {
    
    
    func startObservingUIOrientation() {
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(forName: UIDevice.orientationDidChangeNotification, object: nil, queue: nil) { [weak self] _ in
            self?.setupInterfaceOrientation()
        }
        setupInterfaceOrientation()
    }
    
    
    func setupInterfaceOrientation() {
        guard shouldAutorotate else { return }
        
        switch UIDevice.current.orientation {
        case .landscapeLeft:
            if supportedInterfaceOrientations.contains(.landscapeRight) {
                currentInterfaceOrientation = .landscapeRight
            }
        case .landscapeRight:
            if supportedInterfaceOrientations.contains(.landscapeLeft) {
                currentInterfaceOrientation = .landscapeLeft
            }
        case .portrait:
            if supportedInterfaceOrientations.contains(.portrait) {
                currentInterfaceOrientation = .portrait
            }
        case .portraitUpsideDown:
            if supportedInterfaceOrientations.contains(.portraitUpsideDown) {
                currentInterfaceOrientation = .portraitUpsideDown
            }
        default: break
        }
        
    }
    
    
    func stopObservingUIOrientation() {
        UIDevice.current.endGeneratingDeviceOrientationNotifications()
    }
    
    
}


public extension UIInterfaceOrientation {
    
    
    var rotationFromCamera: CGFloat {
        switch self {
        case .landscapeRight: return .pi
        case .landscapeLeft: return 0
        case .portrait: return .pi * 0.5
        case .portraitUpsideDown: return .pi * 1.5
        case .unknown: return 0
        @unknown default: return 0
        }
    }
    
    
}


