//
//  PopoverConfiguration.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 26/11/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation
import UIKit


public extension UIViewController {
    
    
    func configurePopoverFor(_ vc: UIViewController, presentingFrom sender: Any?, permittedArrowDirections: UIPopoverArrowDirection = [.up, .down]) {
        vc.popoverPresentationController?.permittedArrowDirections = permittedArrowDirections
        
        if let sender = sender as? UIBarButtonItem {
            vc.popoverPresentationController?.barButtonItem = sender
        } else if let sender = sender as? UIView {
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = sender.superview?.convert(sender.frame, to: view) ?? CGRect.zero
        }
    }
    
    
}

