//
//  Autolayout.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 12/03/14.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


extension NSLayoutConstraint {
    
    
    public convenience init(_ item: AnyObject, _ attribute: NSLayoutConstraint.Attribute, _ constant: CGFloat, _ toItem: AnyObject?, _ toItemAttribute: NSLayoutConstraint.Attribute?) {
        self.init(item: item, attribute: attribute, relatedBy: .equal, toItem: toItem, attribute: toItemAttribute ?? .notAnAttribute, multiplier: 1.0, constant: constant)
    }
    
    
    public class func constraintsWithVisualFormats(_ keyedViews: [String : AnyObject], _ formats: String...) -> [NSLayoutConstraint] {
        var results: [NSLayoutConstraint] = []
        for format in formats {
            results.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: format, options: [], metrics: nil, views: keyedViews) )
        }
        return results
    }
    
    
}


/// It lets you set priority inline defining constraint like: activateConstraint( constraint ^ 999 )
public func ^(left: NSLayoutConstraint, right: UILayoutPriority) -> NSLayoutConstraint {
    left.priority = right
    return left
}


extension UIView {
    
    
    public func addConstraintsToFillSuperview(with margin: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(NSLayoutConstraint.constraintsWithVisualFormats(
            ["self" : self],
            "H:|-(==\(margin))-[self]-(==\(margin))-|",
            "V:|-(==\(margin))-[self]-(==\(margin))-|"))
    }
    
    
    public func addConstraintsToAlignTopLeft() {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(NSLayoutConstraint.constraintsWithVisualFormats(
            ["self" : self],
            "H:|-0-[self]",
            "V:|-0-[self]"
            ))
    }
    
    
    public func addConstraintsToFitInSuperview(with margin: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(NSLayoutConstraint.constraintsWithVisualFormats(
            ["self" : self],
            "H:|-(>=\(margin))-[self]-(>=\(margin))-|",
            "V:|-(>=\(margin))-[self]-(>=\(margin))-|"))
    }
    
    
    public func addConstraintsToCenterInSuperview() -> [NSLayoutConstraint] {
        guard let superview = superview else { return [] }
        translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1, constant: 0)
        ]
        NSLayoutConstraint.activate(constraints)
        return constraints
    }
    
    
    public func setContentCompressionResistancePriority(_ priority: UILayoutPriority) {
        setContentCompressionResistancePriority(priority, for: .horizontal)
        setContentCompressionResistancePriority(priority, for: .vertical)
    }
    
    
}
