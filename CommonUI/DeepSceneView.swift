//
//  Created by Jan Mazurczak on 14/04/16.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class DeepSceneView<Subview>: UIView where Subview: UIView {
    
    
    open var adjustInsets: [Bool] = [] {
        didSet {
            setNeedsLayout()
        }
    }
    
    
    /// Set nil to automatically calculate motion amount taking foregroundSubviewAheadOffset as maximum.
    open var motionAmountPerLayer: CGFloat? = 20 {
        didSet {
            setupSubviewsArrangedInDepth()
            setNeedsLayout()
        }
    }
    
    
    open var foregroundSubviewAheadOffset: CGFloat = 0 {
        didSet {
            setupSubviewsArrangedInDepth()
            setNeedsLayout()
        }
    }
    
    
    open var subviewsArrangedInDepth = [Subview]() {
        willSet {
            subviewsArrangedInDepth.forEach { $0.removeFromSuperview() }
        }
        didSet {
            subviewsArrangedInDepth.forEach { addSubview($0) }
            setupSubviewsArrangedInDepth()
            setNeedsLayout()
        }
    }
    
    
    public static func perLayerMotionDelta(count: Int, foregroundMotionOffset: CGFloat) -> CGFloat {
        if count < 2 {
            return 0
        } else {
            return (2 * foregroundMotionOffset) / CGFloat(count - 1)
        }
    }
    
    
    public static func motionAmounts(perLayer: CGFloat, count: Int, foregroundMotionOffset: CGFloat) -> [CGFloat] {
        let deepestMotionAmount: CGFloat
        if count < 2 {
            deepestMotionAmount = 0
        } else {
            deepestMotionAmount = foregroundMotionOffset - (perLayer * CGFloat(count - 1))
        }
        return (0..<count).map { deepestMotionAmount + (CGFloat($0) * perLayer) }
    }
    
    
    fileprivate func enumerateArrangedSubviews(_ action: (_ view: UIView, _ motionAmount: CGFloat, _ index: Int) -> Void) {
        let perLayer: CGFloat
        
        if let motionAmountPerLayer = motionAmountPerLayer {
            perLayer = motionAmountPerLayer
        } else {
            perLayer = DeepSceneView.perLayerMotionDelta(count: subviewsArrangedInDepth.count, foregroundMotionOffset: foregroundSubviewAheadOffset)
        }
        
        let motionAmounts = DeepSceneView.motionAmounts(perLayer: perLayer, count: subviewsArrangedInDepth.count, foregroundMotionOffset: foregroundSubviewAheadOffset)
        
        for i in 0..<subviewsArrangedInDepth.count {
            action(subviewsArrangedInDepth[i], motionAmounts[i], i)
        }
    }
    
    
    fileprivate func setupSubviewsArrangedInDepth() {
        enumerateArrangedSubviews { view, motion, _ in
            view.motionEffects = [DepthMotionEffect(force: motion)]
        }
    }
    
    
    fileprivate func layoutSubviewsArrangedInDepth() {
        enumerateArrangedSubviews { view, motion, index in
            let inset = (index < adjustInsets.count && adjustInsets[index]) ? motion : 0
            view.frame = bounds.insetBy(dx: inset, dy: inset)
        }
    }
    
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        layoutSubviewsArrangedInDepth()
    }
    
    
}
