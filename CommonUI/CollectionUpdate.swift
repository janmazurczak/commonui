//
//  CollectionUpdate.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 28/04/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


public enum CollectionUpdateAnimation {
    
    case delete(index: Int)
    case add(index: Int)
    case update(index: Int)
    case move(fromIndex: Int, toIndex: Int)
    
    public static func with(newResults: NSOrderedSet, oldResults: NSOrderedSet, removedResults: NSOrderedSet, addedResults: NSOrderedSet, changedResults: NSOrderedSet) -> [CollectionUpdateAnimation] {
        
        let oldResultAnimations: [CollectionUpdateAnimation] = removedResults.array.compactMap { removedResult in
            let oldIndex = oldResults.index(of: removedResult)
            guard oldIndex != NSNotFound else { return nil }
            return .delete(index: oldIndex)
        }
        
        let newResultAnimations: [CollectionUpdateAnimation] = addedResults.array.compactMap { addedResult in
            let newIndex = newResults.index(of: addedResult)
            guard newIndex != NSNotFound else { return nil }
            return .add(index: newIndex)
        }
        
        let movedResultAnimations: [CollectionUpdateAnimation] = changedResults.array.compactMap { movedResult in
            let newIndex = newResults.index(of: movedResult)
            let oldIndex = oldResults.index(of: movedResult)
            guard newIndex != NSNotFound else { return nil }
            guard oldIndex != NSNotFound else { return nil }
            guard oldIndex != newIndex   else { return nil }
            return .move(fromIndex: oldIndex, toIndex: newIndex)
        }
        
        let changedResultAnimations: [CollectionUpdateAnimation] = changedResults.array.compactMap { changedResult in
            let index = newResults.index(of: changedResult)
            guard index != NSNotFound else { return nil }
            return .update(index: index)
        }
        
        return oldResultAnimations + changedResultAnimations + newResultAnimations + movedResultAnimations
    }
    
}


extension CollectionUpdateAnimation: Equatable { }

public func ==(lhs: CollectionUpdateAnimation, rhs: CollectionUpdateAnimation) -> Bool {
    switch (lhs, rhs) {
    case let (.delete(left), .delete(right)) where left == right: return true
    case let (.add(left), .add(right)) where left == right: return true
    case let (.update(left), .update(right)) where left == right: return true
    case let (.move(leftFrom, leftTo), .move(rightFrom, rightTo)) where leftFrom == rightFrom && leftTo == rightTo: return true
    default: return false
    }
}


extension CollectionUpdateAnimation: CustomDebugStringConvertible {
    
    public var debugDescription: String {
        switch self {
        case .delete(let i): return "Delete(\(i))"
        case .add(let i): return "Add(\(i))"
        case .update(let i): return "Update(\(i))"
        case .move(let f, let t): return "Move(\(f)->\(t))"
        }
    }
    
}


public protocol UpdatableCollectionController: class {
    
    var collectionView: UICollectionView! { get }
    var items: [Any]? { get set }
    
    func update(newItems: NSOrderedSet, oldItems: NSOrderedSet?, removedResults: NSOrderedSet, addedResults: NSOrderedSet, changedResults: NSOrderedSet, section: Int)
    
}



extension UpdatableCollectionController {
    
    public func update(newItems: NSOrderedSet, oldItems: NSOrderedSet?, removedResults: NSOrderedSet, addedResults: NSOrderedSet, changedResults: NSOrderedSet, section: Int) {
        
        let animations: [CollectionUpdateAnimation]?
        if let oldItems = oldItems {
            animations = CollectionUpdateAnimation.with(newResults: newItems, oldResults: oldItems, removedResults: removedResults, addedResults: addedResults, changedResults: changedResults)
        } else {
            animations = nil
        }
        
        OperationQueue.main.addOperation {
            self.collectionView?.performUpdate({ self.items = newItems.array }, animations: animations, section: section)
        }
    }
    
}


extension UICollectionView {
    
    public func performUpdate(_ update: @escaping () -> Void, animations: [CollectionUpdateAnimation]?, section: Int) {
        if let animations = animations {
            
            var indexPathsNeedingReload = [IndexPath]()
            performBatchUpdates({
                
                indexPathsNeedingReload = self.processAnimations(animations, section: section)
                update()
                
                }, completion: { success in
                    if success { self.reloadItems(at: indexPathsNeedingReload) }
            })
            
        } else {
            update()
            reloadData()
        }
    }
    
    
    fileprivate func processAnimations(_ animations: [CollectionUpdateAnimation], section: Int) -> [IndexPath] {
        
        var indexPathsNeedingReload = [IndexPath]()
        
        for animation in animations {
            switch animation {
            case .add(let row): insertItems(at: [IndexPath(row: row, section: section)])
            case .delete(let row): deleteItems(at: [IndexPath(row: row, section: section)])
            case .move(let from, let to): moveItem(
                at: IndexPath(row: from, section: section),
                to: IndexPath(row: to, section: section))
            case .update(let row): indexPathsNeedingReload += [IndexPath(row: row, section: section)]
            }
        }
        
        return indexPathsNeedingReload
    }
    
}

