//
//  Created in Playgrounds Squire
//  https://itunes.apple.com/pl/app/playgrounds-squire/id1341463239?mt=8
//  Copyright © 2018 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import UIKit

open class CorneringContainer<Background, Header, Body>: UIViewController, UIGestureRecognizerDelegate where Background: UIViewController, Header: UIViewController, Body: UIViewController {
    
    public init(background: Background, header: Header, body: Body) {
        backgroundController = background
        corneringHeaderController = header
        corneringBodyController = body
        super.init(nibName: nil, bundle: nil)
        childrenFromBackToFront.forEach {
            addChild($0)
            $0.didMove(toParent: self)
        }
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public enum State { case hidden, cornered, presented }
    public var state = State.presented {
        didSet {
            switch state {
            case .hidden: break
            case .presented: corneredAmount = 0
            case .cornered: corneredAmount = 1
            }
        }
    }
    
    public let backgroundController: Background
    public let corneringHeaderController: Header
    public let corneringBodyController: Body
    
    private var childrenFromBackToFront: [UIViewController] {
        return [backgroundController, corneringBodyController, corneringHeaderController]
    }
    
    public var presentingHeaderHeight: CGFloat = 200 {
        didSet {
            bodyHeight?.constant = -presentingHeaderHeight
            headerHeight?.constant = presentingHeaderHeight
        }
    }
    
    public var corneringHeaderHeight: CGFloat = 120 {
        didSet {
            recalculateCorneringLayout(for: view.bounds.size)
        }
    }
    
    private var headerHeight: NSLayoutConstraint? {
        willSet { headerHeight?.isActive = false }
        didSet { headerHeight?.isActive = true }
    }
    
    private var bodyHeight: NSLayoutConstraint? {
        willSet { bodyHeight?.isActive = false }
        didSet { bodyHeight?.isActive = true }
    }
    
    private var bodyBottom: NSLayoutConstraint? {
        willSet { bodyBottom?.isActive = false }
        didSet { bodyBottom?.isActive = true }
    }
    
    private var corneringButton: UIButton? {
        willSet { corneringButton?.removeFromSuperview() }
        didSet {
            guard let button = corneringButton else { return }
            button.translatesAutoresizingMaskIntoConstraints = false
            corneringHeaderController.view.addSubview(button)
            if #available(iOSApplicationExtension 11.0, *) {
                button.leftAnchor.constraint(equalTo: corneringHeaderController.view.safeAreaLayoutGuide.leftAnchor, constant: 10).isActive = true
                button.topAnchor.constraint(equalTo: corneringHeaderController.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
            }
            button.addTarget(self, action: #selector(self.corner(button:)), for: .touchUpInside)
        }
    }
    
    private var headerTap: UITapGestureRecognizer? {
        willSet {
            guard let tap = headerTap else { return }
            corneringHeaderController.view.removeGestureRecognizer(tap)
        }
        didSet {
            guard let tap = headerTap else { return }
            tap.delegate = self
            corneringHeaderController.view.addGestureRecognizer(tap)
        }
    }
    
    private var headerPan: UIPanGestureRecognizer? {
        willSet {
            guard let pan = headerPan else { return }
            corneringHeaderController.view.removeGestureRecognizer(pan)
        }
        didSet {
            guard let pan = headerPan else { return }
            pan.delegate = self
            corneringHeaderController.view.addGestureRecognizer(pan)
        }
    }
    
    private var bodyPan: UIPanGestureRecognizer? {
        willSet {
            guard let pan = bodyPan else { return }
            corneringBodyController.view.removeGestureRecognizer(pan)
        }
        didSet {
            guard let pan = bodyPan else { return }
            pan.delegate = self
            corneringBodyController.view.addGestureRecognizer(pan)
        }
    }
    
    private var corneredAmount: CGFloat = 0 {
        didSet {
            recalculateCorneringLayout(for: view.bounds.size)
        }
    }
    
    private var presentedCorneredAmount: CGFloat {
        let bottom = corneringBodyController.view.layer.presentation()?.frame.maxY ?? corneringBodyController.view.frame.maxY
        return 1 - (bottom / view.bounds.size.height)
    }
    
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        UIView.animate(withDuration: coordinator.transitionDuration) {
            self.recalculateCorneringLayout(for: size)
        }
    }
    
    private func recalculateCorneringLayout(for size: CGSize) {
        bodyBottom?.constant = -corneredAmount * size.height
        view.layoutIfNeeded()
        let cornerMargin: CGFloat
        if #available(iOSApplicationExtension 11.0, *) {
            cornerMargin = max(view.directionalLayoutMargins.top, view.directionalLayoutMargins.trailing) * sqrt(2)
        } else {
            cornerMargin = 0
        }
        corneringHeaderController.view.transform = CGAffineTransform
            .identity
            .translatedBy(x: size.width * 0.5 * corneredAmount, y: presentingHeaderHeight * -0.5 * corneredAmount)
            .rotated(by: 0.25 * .pi * corneredAmount)
            .translatedBy(x: 0, y: (corneringHeaderHeight + cornerMargin - (presentingHeaderHeight * 0.5)) * corneredAmount)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        childrenFromBackToFront.forEach {
            $0.view.translatesAutoresizingMaskIntoConstraints = false
            $0.view.isUserInteractionEnabled = true
            view.addSubview($0.view)
        }
        
        backgroundController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundController.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        corneringBodyController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        corneringBodyController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bodyHeight = corneringBodyController.view.heightAnchor.constraint(equalTo: view.heightAnchor, constant: -presentingHeaderHeight)
        bodyBottom = corneringBodyController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -corneredAmount * view.bounds.size.height)
        
        corneringHeaderController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        corneringHeaderController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        corneringHeaderController.view.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        headerHeight = corneringHeaderController.view.heightAnchor.constraint(equalToConstant: presentingHeaderHeight)
        
        headerPan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(gesture:)))
        bodyPan = UIPanGestureRecognizer(target: self, action: #selector(self.pan(gesture:)))
        headerTap = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        corneringButton = UIButton()
        corneringButton?.setImage(UIImage(named: "CorneringButton"), for: .normal)
    }
    
    open func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let t = pan.translation(in: view)
            return (state == .presented && t.y < 0) || state == .cornered
        }
        if gestureRecognizer is UITapGestureRecognizer {
            return state == .cornered
        }
        return true
    }
    
    @objc func corner(button: UIButton) {
        animate(to: .cornered)
    }
    
   @objc func tap(gesture: UIPanGestureRecognizer) {
        animate(to: .presented)
    }
    
    @objc func pan(gesture: UIPanGestureRecognizer) {
        if gesture.state == .began {
            gesture.setTranslation(CGPoint(x: 0, y: presentedCorneredAmount * -view.bounds.size.height), in: view)
        }
        var y = gesture.translation(in: view).y
        if gesture.state == .ended || gesture.state == .cancelled {
            let velocity = gesture.velocity(in: view).y
            let referenceVelocity = velocity * view.bounds.size.height
            y += (referenceVelocity * 0.001)
            let corneredAmount = -y / view.bounds.size.height
            let state: State = (corneredAmount > 0.5) ? .cornered : .presented
            animate(to: state, normalisedVelocity: abs(velocity * 0.0002))
        } else {
            corneredAmount = max(0, min(-y / view.bounds.size.height, 1))
        }
    }
    
    public func animate(to state: State) {
        animate(to: state, normalisedVelocity: 0)
    }
    
    private func animate(to state: State, normalisedVelocity: CGFloat) {
        UIView.animate(
            withDuration: TimeInterval(max(0.2, min(1 - normalisedVelocity, 0.8))),
            delay: 0,
            usingSpringWithDamping: 0.6,
            initialSpringVelocity: normalisedVelocity,
            options: [.allowUserInteraction],
            animations: { self.state = state },
            completion: nil)
    }
    
}
