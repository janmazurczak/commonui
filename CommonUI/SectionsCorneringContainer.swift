//
//  Created in Playgrounds Squire
//  https://itunes.apple.com/pl/app/playgrounds-squire/id1341463239?mt=8
//  Copyright © 2018 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


import UIKit

open class SectionsCorneringContainer<Background, Header>: CorneringContainer<Background, Header, HorizontalSectionsController>, UnderlinedSectionLabelDelegate where Background: UIViewController, Header: UIViewController {
    
    public enum Style {
        case sectionLabelsOnHeader
        case sectionLabelsOnBody
    }
    
    public let style: Style
    
    public init(background: Background, header: Header, sections: [UIViewController], style: Style) {
        let body = HorizontalSectionsController()
        body.sections = sections
        self.style = style
        super.init(background: background, header: header, body: body)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        switch style {
        case .sectionLabelsOnHeader:
            moveLabelsToHeader()
        case .sectionLabelsOnBody: break
        }
    }
    
    private func moveLabelsToHeader() {
        guard let labels = corneringBodyController.labels else { return }
        guard let view = corneringHeaderController.view else { return }
        view.addSubview(labels)
        labels.layer.zPosition = 1000
        labels.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        labels.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        labels.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        labels.heightAnchor.constraint(equalToConstant: 40).isActive = true
        labels.delegate = self
    }
    
    public func didSelect(underlinedSection: UnderlinedHorizontalSectionLabel, index: Int) {
        corneringBodyController.didSelect(underlinedSection: underlinedSection, index: index)
        if state == .cornered { animate(to: .presented) }
    }
    
}

