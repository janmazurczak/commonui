//
//  Copyright (c) 2015 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


public protocol AspectRatioSensitiveHorizontalLayoutDataSource: class {
    /// width/height
    func layout(_ layout: AspectRatioSensitiveHorizontalLayout, aspectRatioAtIndexPath indexPath: IndexPath) -> CGFloat?
}

open class AspectRatioSensitiveHorizontalLayout: UICollectionViewLayout {
    
    
    fileprivate var collectionViewBounds = CGRect.zero
    fileprivate var cellMaxSize = CGSize.zero
    fileprivate var contentSize = CGSize.zero
    fileprivate var currentOffset: CGFloat = 0.0
    
    
    fileprivate(set) var focusedIndex: Int = 0
    fileprivate var focusedElementXPosition: CGFloat = 0.0
    
    
    /// Offscreen cells preloaded on each side of visible cells
    open var includeOffscreenCells: Int = 2
    open weak var dataSource: AspectRatioSensitiveHorizontalLayoutDataSource?
    
    
    open override func prepare() {
        cachedElementSizes = [:]
        cachedElementXPos = [:]
        collectionViewBounds = collectionView?.bounds ?? CGRect.zero
        cellMaxSize = CGSize(
            width: collectionViewBounds.size.width - (2 * cellsMargin),
            height: collectionViewBounds.size.height - (2 * cellsMargin))
        contentSize = CGSize(
            width: CGFloat(collectionView?.numberOfItems(inSection: 0) ?? 0) * cellMaxSize.width + (2 * cellsMargin),
            height: cellMaxSize.height)
        currentOffset = collectionViewBounds.origin.x / cellMaxSize.width
        focusedIndex = Int(round(currentOffset))
        
        let offsetPercentage = currentOffset - CGFloat(focusedIndex)
        let estimatedPosition = (CGFloat(focusedIndex) + 0.5) * cellMaxSize.width
        focusedElementXPosition = estimatedPosition + offsetPercentage * (cellMaxSize.width - 0.5 * (elementSize(at: focusedIndex).width + elementSize(at: offsetPercentage > 0.0 ? focusedIndex + 1 : focusedIndex - 1).width))
    }
    
    
    open override var collectionViewContentSize: CGSize {
        return contentSize
    }
    
    
    fileprivate var cachedElementSizes: [Int:CGSize] = [:]
    fileprivate func elementSize(at index: Int) -> CGSize {
        
        if collectionView == nil || index < 0 || index >= collectionView!.numberOfItems(inSection: 0) {
            cachedElementSizes[index] = cellMaxSize
            return cellMaxSize
            
        } else if let size = cachedElementSizes[index] {
            return size
            
        } else if let aspectRatio = dataSource?.layout(self, aspectRatioAtIndexPath: IndexPath(item: index, section: 0)) {
            
            let size = CGSize(width: aspectRatio, height: 1)
            let scale = min(cellMaxSize.width / size.width, cellMaxSize.height / size.height)
            
            cachedElementSizes[index] = size * scale
            return size * scale
            
        } else {
            cachedElementSizes[index] = cellMaxSize
            return cellMaxSize
            
        }
    }
    
    
    open var cellsPadding: CGFloat = 8.0
    open var cellsMargin: CGFloat = 0.0
    
    
    fileprivate var cachedElementXPos: [Int:CGFloat] = [:]
    fileprivate func elementXPosition(at index: Int) -> CGFloat {
        if index == focusedIndex {
            return focusedElementXPosition
        } else if let pos = cachedElementXPos[index] {
            return pos
        } else {
            let nextMultiplier: CGFloat = index < focusedIndex ? -0.5 : 0.5
            let nextIndex = index < focusedIndex ? index + 1 : index - 1
            let pos = elementXPosition(at: nextIndex) + nextMultiplier * (elementSize(at: index).width + elementSize(at: nextIndex).width + cellsPadding)
            cachedElementXPos[index] = pos
            return pos
        }
    }
    
    
    open override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        
        attributes.size = elementSize(at: indexPath.item)
        
        // x needs to be limited to touch collection view bounds edge to force collection view to load offscreen cells
        let correctOriginalX = elementXPosition(at: indexPath.item) + cellsMargin
        let x = min(max(
            collectionViewBounds.origin.x - attributes.size.width * 0.5 + 1.0,
            correctOriginalX),
            collectionViewBounds.origin.x + collectionViewBounds.size.width + attributes.size.width * 0.5 - 1.0
            )
        
        let focusedFactor = 1.0 - abs(x - collectionViewBounds.midX) / cellMaxSize.width
        
        attributes.center = CGPoint(x: x, y: collectionViewBounds.size.height * 0.5)
        attributes.alpha = (correctOriginalX == x) ? max(0.6, focusedFactor) : 0.0
        attributes.zIndex = Int(focusedFactor * 10000)
        return attributes
    }
    
    
    open override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var results: [UICollectionViewLayoutAttributes] = []
        if let cv = collectionView {
            var includedOffscreenCellsCount: Int = 0
            let itemsCount = cv.numberOfItems(inSection: 0)
            
            var i = focusedIndex-1
            while i >= 0 {
                let halfWidth = elementSize(at: i).width * 0.5
                let posX = elementXPosition(at: i)
                let offscreen = posX + halfWidth < rect.origin.x
                if offscreen && includedOffscreenCellsCount >= includeOffscreenCells {
                    break
                } else if (posX - halfWidth < rect.origin.x + rect.size.width) && (i < itemsCount) {
                    if offscreen { includedOffscreenCellsCount += 1 }
                    results.append(layoutAttributesForItem(at: IndexPath(item: i, section: 0))!)
                }
                i -= 1
            }
            includedOffscreenCellsCount = 0
            for i in focusedIndex ..< itemsCount {
                let halfWidth = elementSize(at: i).width * 0.5
                let posX = elementXPosition(at: i)
                let offscreen = posX - halfWidth > rect.origin.x + rect.size.width
                if offscreen && includedOffscreenCellsCount >= includeOffscreenCells {
                    break
                } else if (posX + halfWidth > rect.origin.x) && (i >= 0) {
                    if offscreen { includedOffscreenCellsCount += 1 }
                    results.append(layoutAttributesForItem(at: IndexPath(item: i, section: 0))!)
                }
            }
        }
        return results
    }
    
    
    open override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    
    open override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        return CGPoint(x: round(proposedContentOffset.x / cellMaxSize.width) * cellMaxSize.width, y: proposedContentOffset.y)
    }
    
}
