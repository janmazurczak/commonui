//
//  ListAndItemNavigationController.swift
//
//  Created by Jan Mazurczak on 26/04/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import CommonElements


open class ListAndItemNavigationController: UINavigationController {
    
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone: return .allButUpsideDown
        case .pad: return .all
        default: return .all
        }
    }
    
    
    /// Set it in case you want to support only one type of VC to present an item. If you need to support more than one type of VC for item presenting: check itemPresenterViewControllerStoryboardID(forItem item: Any) -> String instead
    open var defaultItemPresenterViewControllerStoryboardID: String = "ItemPresenter"
    
    
    /// Override to return storyboard ID for presenter depending on items type.
    open func itemPresenterViewControllerStoryboardID(forItem item: Any) -> String {
        return defaultItemPresenterViewControllerStoryboardID
    }
    
    
    open func itemPresenterViewController(forItem item: Any) -> UIViewController? {
        let vc = storyboard?.instantiateViewController(withIdentifier: itemPresenterViewControllerStoryboardID(forItem: item))
        (vc as? ItemPresenter)?.item = item
        return vc
    }
    
    
    open weak var flow: ListAndItemFlow? {
        willSet {
            flow?.stopObserving(by: self)
        }
        didSet {
            flow?.observe(by: self, queue: .main) {
                $1.presentSelectedItemIfNeeded(animated: true)
            }
            presentSelectedItemIfNeeded(animated: false)
        }
    }
    
    
    fileprivate func presentSelectedItemIfNeeded(animated: Bool) {
        if let selectedItem = flow?.selectedItem {
            
            if let itemVC = viewControllers.last as? ItemPresenter {
                itemVC.item = selectedItem
            } else if let itemVC = itemPresenterViewController(forItem: selectedItem as Any) {
                setViewControllers([viewControllers.first].compactMap{ $0 } + [itemVC], animated: animated)
            }
            
        } else {
            popToRootViewController(animated: animated)
        }
    }
    
    
}
