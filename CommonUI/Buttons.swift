//
//  Buttons.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 24/04/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


extension UIButton {
    
    public class func ActionKeyedButtons(_ keys: [String], target: AnyObject, creator: (String) -> UIButton) -> [String: UIButton] {
        var results: [String: UIButton] = [:]
        for key in keys {
            let actionName = Selector("action" + key.capitalized + ":")
            let button = creator(key)
            button.addTarget(target, action: actionName, for: .touchUpInside)
            results[key] = button
        }
        return results
    }
    
}


extension UIButton {
    
    public func setupStandardBorder() {
        layer.borderWidth = 1
        layer.borderColor = tintColor.cgColor
        layer.cornerRadius = 5
    }
    
}


extension UIButton {
    
    public func disable(for duration: TimeInterval) {
        isEnabled = false
        isUserInteractionEnabled = false
        let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(duration * 1000))
        DispatchQueue.main.asyncAfter(deadline: deadline) { [weak self] in
            self?.isEnabled = true
            self?.isUserInteractionEnabled = true
        }
    }
    
}


open class UIButtonWithExtendedHitTestArea: UIButton {
    
    
    open var minimumTappableSize: CGFloat = 60
    
    
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let sizeDelta = CGSize(
            width: max(0, (minimumTappableSize - bounds.size.width)),
            height: max(0, (minimumTappableSize - bounds.size.height)))
        return CGRect(
            x: bounds.origin.x - 0.5 * sizeDelta.width,
            y: bounds.origin.y - 0.5 * sizeDelta.height,
            width: bounds.size.width + sizeDelta.width,
            height: bounds.size.width + sizeDelta.width).contains(point) ? self : nil
    }
    
    
    open func addTargetForTap(target: AnyObject?, action: Selector) {
        addTarget(target, action: action, for: [.touchUpInside, .touchUpOutside])
    }
    
    
    open var intrinsicContentSizeModifier = CGSize.zero
    
    
    open override var intrinsicContentSize: CGSize {
        get {
            let originalSize = super.intrinsicContentSize
            return CGSize(
                width: originalSize.width + intrinsicContentSizeModifier.width,
                height: originalSize.height + intrinsicContentSizeModifier.height)
        }
    }
    
}

