//
//  RoundLabel.swift
//  CommonUI
//
//  Created by Jan Mazurczak on 23/08/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit


open class RoundLabel: UIView {
    
    
    open var text: NSAttributedString? = nil {
        didSet { setNeedsDisplay() }
    }
    open var startAngle: CGFloat? = nil {
        didSet { setNeedsDisplay() }
    }
    open var margin: CGFloat = 12 {
        didSet { setNeedsDisplay() }
    }
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupOnce()
    }
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupOnce()
    }
    
    
    private func setupOnce() {
        contentMode = .redraw
        isUserInteractionEnabled = false
        backgroundColor = .clear
    }
    
    
    private func calculateStartAngleFromTextLength() -> CGFloat {
        guard let text = text else { return 0 }
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        
        var result: CGFloat = 0
        for i in 0..<text.length {
            let characterToRender = text.attributedSubstring(from: NSRange(location: i, length: 1))
            let characterSize = characterToRender.size()
            result += characterSize.width / (center.x - characterSize.height)
        }
        
        return result * -0.5
    }
    
    
    open override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        guard let text = text else { return }
        
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        var angle = startAngle ?? calculateStartAngleFromTextLength()
        
        for i in 0..<text.length {
            let characterToRender = text.attributedSubstring(from: NSRange(location: i, length: 1))
            let characterSize = characterToRender.size()
            let font = characterToRender.attributes(at: 0, effectiveRange: nil)[NSAttributedString.Key.font] as? UIFont
            
            angle += (characterSize.width * 0.5) / (center.x - characterSize.height)
            
            context.saveGState()
            context.translateBy(x: center.x, y: center.y)
            context.rotate(by: angle)
            
            characterToRender.draw(at: CGPoint(x: -0.5 * characterSize.width, y: -center.y + margin - (font?.ascender ?? 0)))
            
            context.restoreGState()
            
            angle += (characterSize.width * 0.5) / (center.x - characterSize.height)
        }
    }
    
    
}
