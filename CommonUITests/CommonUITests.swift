//
//  CommonUITests.swift
//  CommonUITests
//
//  Created by Jan Mazurczak on 12/03/16.
//  Copyright © 2016 Jan Mazurczak. All rights reserved.
//

import XCTest
@testable import CommonUI

class CommonUITests: XCTestCase {
    
    
    func testUserInterfaceAnimatingOutAndInFast() {
        let expectation = self.expectation(description: "Do not remove subviews when animation should end in case they were added again")
        
        let window = UIWindow()
        let viewA = UIView()
        let userInterface = UserInterfaceExample(traitCollection: UITraitCollection())
        
        window.addSubview(viewA)
        userInterface.present(in: viewA)
        XCTAssertEqual(viewA.subviews.count, 3)
        OperationQueue().addOperation {
            Thread.sleep(forTimeInterval: 0.6)
            OperationQueue.main.addOperation {
                userInterface.dismiss()
            }
            Thread.sleep(forTimeInterval: 0.2)
            OperationQueue.main.addOperation {
                userInterface.present(in: viewA)
                XCTAssertEqual(viewA.subviews.count, 6)
            }
            Thread.sleep(forTimeInterval: 0.1)
            OperationQueue.main.addOperation {
                XCTAssertEqual(viewA.subviews.count, 6)
            }
            Thread.sleep(forTimeInterval: 0.5)
            OperationQueue.main.addOperation {
                XCTAssertEqual(viewA.subviews.count, 3)
                userInterface.dismiss()
            }
            Thread.sleep(forTimeInterval: 0.7)
            OperationQueue.main.addOperation {
                XCTAssertEqual(viewA.subviews.count, 0)
            }
            
            OperationQueue.main.addOperation {
                userInterface.present(in: viewA)
                userInterface.present(in: viewA)
                userInterface.present(in: viewA)
                XCTAssertEqual(viewA.subviews.count, 3)
            }
            Thread.sleep(forTimeInterval: 0.2)
            
            OperationQueue.main.addOperation {
                XCTAssertEqual(viewA.subviews.count, 3)
            }
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 3, handler: nil)
    }
    
    class UserInterfaceExample: UserInterface {
        override func createSubviews() -> [String : UIView] {
            return ["view":UIView(), "button":UIButton(), "image":UIImageView()]
        }
    }
    
}


extension UIView: FadeInAndOut {
    public func fadeIn(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        fadeInLikeMistyGhost(animationSettings, completion: completion)
    }
    public func fadeOut(_ animationSettings: AnimationSettings?, completion: ((Bool) -> Void)?) {
        fadeOutLikeMistyGhost(animationSettings, moveBy: .zero, completion: completion)
    }
}

